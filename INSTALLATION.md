# INSTALLATION D'ALECSIA

## Installation avec Docker

Ajoutez éventuellement des comptes et mots de passe dans le fichier
`docker/alecsia/conf/passwords.yml`. Ces comptes doivent soit correspondre à
des comptes enseignants (et doivent donc être présents dans le teacherbook qui
est initialisé avec le fichier SQL `docker/dump.sql`) soit correspondre à des
comptes étudiants (pour l'instant il n'y a pas la possibilité de les créer
automatiquement il faut les créer à la main une fois le teacherbook installé).

Éditez éventuellement les volumes pour les mettre à l'endroit qui vous
convient et faites en sorte que les répertoires de logs et d'upload soit
accessibles en écriture par tout le monde.

Une fois cette configuration faite on peut lancer les conteneurs en se rendant
dans le répertoire `docker`, puis :
```
docker-compose up -d
```

Then the Alecsia web application should be available at:
http://localhost:8080/alecsia/web/app_dev.php (with usernames/passwords as set
in `passwords.yml`.

The teacherbook (used by Alecsia) is accessible through: http://localhost:8080/teacherbook/ (for instance: http://localhost:8080/teacherbook/teacher/hopper should return the user Grace Hopper).

## Installation sans Docker

Télécharger Alecsia ne suffit pas : ce dossier n'est en effet pas fourni avec l'ensemble
des composants permettant de faire fonctionner l'application. Ces fichiers, les "vendors"
, sont des plug-ins qu'il ne serait pas judicieux d'inclure dans un dépôt git. 

### Logiciels pré-requis

Par ailleurs Alecsia a certains pré-requis technologiques : 
- PHP 5.5+. Alecsia ne fonctionne pas avec les versions récentes de PHP (par ex. PHP 7)
- PHP-curl
- Composer (http://getcomposer.org)
- phpunit
- apache
- git
  - bsdtar
- MySQL
- Apache 2

Dans le cas où vous utilisez un système basé sur Debian/Ubuntu vous pouvez
essayer (à vos risques et périls) le script `server_install.sh`.

#### Script d'installation du serveur

Durant l'installation, il vous faudra rentrer un mot de passe pour
l'administrateur de la base de données. C'est celui-là qui vous sera utile
pour que PhpMyAdmin puisse s'y connecter.

La connexion à PhpMyAdmin doit être possible depuis l'adresse `http://localhost/phpmyadmin`.

### Installation d'Alecsia
De plus, une phase de configuration est nécessaire pour faire fonctionner Alecsia.

Le présent document donne donc une marche à suivre afin de parvenir à obtenir une copie
d'Alecsia exécutable sur votre machine.

Mettez-vous dans un répertoire de travail.

#### Installation du teacherbook

Alecsia repose sur une liste des enseignants connus afin de savoir si la
personne qui vient de se connecter est enseignante ou étudiante.

Lancez la commande :
```
git clone https://git.framasoft.org/mikaels/teacherbook.git
```

Allez dans le répertoire `teacherbook` et lancez la commande :
```
composer install
```
pour installer les composants nécessaires à l'application.

#### Configurer le teacherbook

Le fichier `config.php.example` contient un exemple de fichier de
configuration. Copiez-le et renommez-le en `config.php` et modifiez les
informations qu'il contient pour correspondre aux informations de connexion à
votre base de données.

Il est aussi nécessaire de créer une base dans votre base de données, dont le
nom sera le nom  indiqué dans la constante `DB_NAME` du fichier
`config.php`.

Peuplez la base de données avec le fichier `dump.sql` (il est par exemple
possible d'importer des données SQL depuis PhpMyAdmin, sinon cela peut être
fait en ligne de commande).

#### Se conecter à son teacherbook

### I. TÉLÉCHARGEMENT DES VENDORS

>  Symfony utilise composer. Lancer 'composer install' à la racine. 
   Il téléchargera les vendors : cela devrait prendre quelques minutes.
   
>  Vérifiez qu'un dossier "vendor" a été créé à la racine du dossier Alecsia.


### II. CONFIGURATION

>  Tentez de lancer http://<chemin_vers_alecsia>/web/config.php
   Ce fichier a pour but de tester si la configuration du serveur est correcte.
   Si des "Major Problem" s'affichent, corrigez les : classiquement, il s'agira 
   d'accorder des permissions aux dossiers 'app/cache' et 'app/logs'.

>  Les "Minor Problem" sont ici à des fin d'optimisation. Ils peuvent être ignorés dans
   un premier temps, mais il peut être judicieux d'y revenir par la suite pour améliorer
   les performances de l'application.
   
>  Copiez le fichier 'parameters.yml.example' dans 'app/config' et renommez le en
   'parameters.yml'. Complétez les informations relatives à la base de données. Enregistrez.

>  Au niveau d'Apache (configuration du Virtual Host ou configuration du répertoire), il 
   faut spécifier la variable d'environnement LANG (selon la valeur du système) :
   SetEnv LANG en_US.UTF-8
   
> Au niveau de la configuration PHP. Permettre l'upload de fichiers suffisamment
  gros. Il faut donc modifier les valeurs de la constante upload_max_filesize
  et post_max_size dans le fichier de configuration PHP (par exemple dans `/etc/php5/apache2/php.ini`)

> Il faut potentiellement modifier les adresses d'accès au CAS dans le fichier `app/config/config.yml` (et éventuellement les attributs d'accès dans `src/Alecsia/AnnotationBundle/Security/User/Lille1/Lille1UserProvider.php`)

> Le répertoire où les travaux d'étudiants seront uploadés est déterminé par le chemin spécifié dans le fichier `app/config/parameters.yml` sous la variable `alecsia.upload_dir`. Ce répertoire doit être accessible en écriture par le serveur web.

### III. CRÉATION DE LA STRUCTURE DE BDD

>  Exécutez 'app/console doctrine:schema:create'
   Si tout se passe bien, la commande remplira la base de données avec les
   tables prévues.

>  Si vous venez d'une précédente installation de Alecsia, il s'agira alors
   de lancer 'app/console doctrine:schema:update'.

### IV. TEACHER BOOK

> Installer le teacher book (récupérer l'application sur le dépôt https://git.framasoft.org/mikaels/teacherbook)

> Rendre l'application accessible via une URL

> Modifier la valeur de la variable de configuration alecsia.lille1.teacherbook 
  dans le fichier parameters.yml

### V. PHPUNIT

> Le lancement des tests unitaires nécessite l'installation de phpunit

> Sous certains Linux il semble nécessaire de lancer les commandes suivantes
sudo pear config-set auto_discover 1
sudo pear install pear.phpunit.de/PHPUnit

> Ensuite on peut tranquillement faire à la racine un 
phpunit -c app/

### VI. Déployer l'application

Il devrait être nécessaire de faire un :
```
app/console assets:install
```

### VII. ACCÉDER À L'APPLICATION

Il suffit d'accéder au fichier `web/app.php` (pour la version production) ou à `web/app_dev.php` (pour la version développement).
Attention la version production nécessite l'accès au SSO/CAS de Lille 1 alors que pour la version développement l'authentification ne se fait que par un fichier (pour plus de détail voir la partie « Authentification ») du fichier [doc/dev.org](doc/dev.org).  

### QUE FAIRE SI ALECSIA NE MARCHE PLUS APRES UN PULL

>  Lancez le script 'composer update'

### APRÈS UNE MISE À JOUR DE SYMFONY

```
php ./vendor/sensio/distribution-bundle/Sensio/Bundle/DistributionBundle/Resources/bin/build_bootstrap.php
```
