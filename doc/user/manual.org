#+TITLE:Manuel d'utilisateur de Alecsia
#+STYLE: <link rel="stylesheet" type="text/css" href="manual.css" />
#+LATEX_CMD: pdflatex
#+EXCLUDE_TAGS: dev
#+AUTHOR: 
* Débuter
  Lorsqu'on commence avec une nouvelle instance d'Alecsia on arrive sur une
  page vierge permettant de créer une nouvelle UE.

  On note aussi la présence d'un menu à votre nom, en haut à droite, qui
  permet de paramétrer le comportement d'Alecsia vis-à-vis des rendus des
  étudiants :
  - Gestion des modèles :: Permet de créer des modèles d'annotations (des
       commentaires sur les TP) qui pourront être réutilisés dans n'importe
       quel TP de n'importe quelle matière. D'autres modèles pourront être
       créés plus tard, mais ils ne seront utilisables qu'au sein d'une UE ou
       alors au sein d'un TP.
  - Gestion des règles :: Définit la façon dont les archives des étudiants
       sont traités ainsi que les codes sources qu'il faut reconnaître.
       Plus de détails sont donnés dans la [[rgl_import][section idoine]].
       
  
  [[file:images/pageVierge.jpg]]

* UE

  L'ajout d'une nouvelle UE se fait en cliquant sur /nouvelle UE/.
  Toute UE est propre à une année,  possède un nom court, et un nom détaillé plus long. 
  Une fois que
  des UE auront été créées, il sera possible de les recopier (d'une année
  sur l'autre, par exemple) ainsi que l'ensemble des sujets.

  Une fois l'UE créée, celle-ci est composée de
  deux panneaux. Un pour les travaux pratiques (avec rendu de fichiers, donc)
  et un autre pour les interrogations écrites pour lesquelles il n'y a pas de rendu de
  fichiers.

  [[file:images/ue.jpg]]

  Toute UE est composée par défaut d'un groupe, il est possible d'en ajouter
  par la suite.

  Plusieurs boutons sont à disposition sur la droite:
  - Nouvelle UE :: Création d'une nouvelle UE
  - Gérer l'UE :: Permet de modifier des paramètres de l'UE :
    - Renommer :: Nouveau nom de l'UE
    - Archiver :: L'UE n'est plus d'actualité et on désire la mettre
                  dans les archives (où il sera possible de la supprimer, si
                  nécessaire).
    - Créer un groupe :: Ajoute un nouveau groupe. À partir du moment où un
         nouveau groupe est créé, les groupes doivent avoir un nom afin de les
         différencier.
    - Supprimer :: Supprimer l'UE (généralement en cas d'erreur ou d'essais).
                   *Attention* la suppression supprime également tout le
                   contenu de l'UE.
    - Exporter les notes :: Exporte l'ensemble des notes d'une UE au sein d'un
         fichier CSV contenant les informations de login, nom, prénom, groupe
         des étudiants.
  - Gérer le groupe :: Apparaît autant de fois qu'il y a de groupe.
    - Clé de partage :: Il s'agit de la clé à communiquer aux étudiants
                        afin de les inscrire dans le groupe de cette UE.
    - Étudiants non inscrits :: N'apparaît que dans le cas où vous avez ajouté des étudiants non inscrits (voir [[gestion_etudiants_rendu][après]]).
    - Interdire/Rétablir l'accès :: Dans certains cas, il peut être utile que
         les étudiants n'aient plus accès aux rendus d'une UE (par exemple
         lors d'un contrôle de TP). Il est alors possible de leur empêcher
         tout accès à leurs rendus en cliquant sur « Interdire l'accès ». Par
         la suite, il ne faudra pas oublier de leur ré-autoriser la
         consultation en cliquant sur « Rétablir l'accès ». Cela affecte tous
         les étudiants d'un groupe.
** Export des notes                                                     :dev:
   Voir le contrôleur [[file:../../src/Alecsia/AnnotationBundle/Controller/Export/NotesExporterController.php][NotesExporterController.php]] et le fichier de routage
   [[file:../../src/Alecsia/AnnotationBundle/Resources/config/routing/export.yml][export.yml]].  La récupération des notes en elle-même est réalisée dans les
   entités (soit [[file:~/Enseignement/PJI/2011--2012/alecsia/lille1/alecsia/src/Alecsia/AnnotationBundle/Entity/Sujet.php][Sujet]] soit [[file:../../src/Alecsia/AnnotationBundle/Entity/UE.php][UE]]).  La réalisation des CSV est dédiée à deux
   vues une pour les [[file:../../src/Alecsia/AnnotationBundle/Resources/views/Notes/notes.csv.twig][notes par sujet]] une autre pour les [[file:../../src/Alecsia/AnnotationBundle/Resources/views/Notes/notes_ue.csv.twig][notes par UE]].
** Ajout d'étudiants
   Ce n'est pas à vous d'ajouter les étudiants à un groupe, mais aux étudiants de venir s'inscrire.
   Ils ont pour cela besoin de la clé d'inscription (accessible depuis le menu « Gérer un groupe »).

   L'interface que voient les étudiants lors de leur première connexion à Alecsia est celle-ci : 

   [[file:images/ue_student.jpg]]

   Ils disposent d'un bouton en haut à droite « Rejoindre une UE » qui leur
   permet de saisir la clé d'inscription fournie par l'enseignant.

   Le menu à leur nom, également en haut à droite, dispose d'un bouton « Mes
   informations » qui leur permet de préciser leurs nom, prénom et adresse
   mail (qui ne sont pas automatiquement renseignées).  Ces informations
   doivent être *impérativement renseignées* si vous souhaitez qu'Alecsia
   identifie les rendus des étudiants en utilisant leur nom de famille (au
   lieu du login Lille 1 — généralement de la forme prenom.nom — utilisé par
   défaut).

** Ajout d'un sujet
   L'ajout d'un sujet est indispensable pour pouvoir déposer des rendus et les
   corriger.

   Dans la page de l'UE, il suffit de cliquer sur le bouton /Nouveau sujet/.

   Il faut commencer par donner un nom au TP ou à l'interrogation écrite ainsi
   que définir sur combien de points est noté ce travail. Une fois les
   informations données, nous sommes redirigés sur la page de ce nouveau TP.
   
   [[file:images/sujet_vide.jpg]]
*** Gestion du sujet
    Un sujet dispose lui aussi d'un menu /Gérer/ :
    - Rendre consultable :: Permet aux étudiants de voir leurs rendus
         (corrigés ou non). Tant qu'un sujet n'est pas consultable, il est
         impossible pour les étudiants d'accéder à leurs rendus.  Le fait
         qu'un rendu ne soit pas consultable est matérialisé par la petite
         icône bleue à droite du nom du sujet (c'est censé représenter un œil
         fermé…).
    - Renommer :: Donner un nouveau nom au sujet
    - Supprimer :: Supprimer le sujet, ainsi que *l'ensemble des rendus* qui
                   le compose (dans tous les groupes).
    - Affecter les rendus sans étudiant :: Une détection automatique, fondée
         sur le nom du fichier, permet d'associer un rendu à un ou plusieurs
         étudiants. Dans le cas où des étudiants s'inscrivent tardivement, il
         est possible de relancer cette détection automatique, uniquement sur
         les rendus qui sont encore sans étudiant.
    - Geler les notes :: Le gel de notes consiste à figer les notes de sorte
         qu'elles ne puissent plus être modifiées. En effet les modèles
         d'annotation utilisés dans les différents TP peuvent être modifiés en
         cours de route. Si on choisit de modifier un modèle cela peut
         impacter des TP qui ont déjà été corrigés il y a plusieurs
         semaines. Pour éviter de modifier les notes de ces TP-là il faut
         choisir de geler les notes. Cela bloquera les notes dans leur état
         actuel, quelles que soient les modifications réalisées sur les
         modèles utilisés. À l'inverse il n'est pas possible de revenir sur
         des notes qui ont été gelées.
*** Définition du barème
    Pour définir un barème, il faut cliquer sur le bouton /Gérer les
    exercices/. Ce même bouton permettra, par la suite, de modifier le barème.
    S'ensuit l'ouverture d'une fenêtre modale, permettant d'entrer le nom de
    l'exercice (première colonne), le nombre de points de cet exercice
    (deuxième colonne), le nombre de points bonus ou malus (troisième
    colonne), puis le mode de notation (numérique ou un barème de lettres).
    En bas de la fenêtre, est défini le nombre de points total du TP. Si la
    somme des points des exercices ne correspond pas à la note totale du TP,
    le barème est automatiquement ajusté afin de faire correspondre les notes
    des exercices à la note du TP (dans le cas où ce comportement n'est pas
    souhaité, il faut décocher la case en bas à droite).

    [[file:images/sujet_bareme.jpg]]

    Dans l'exemple ci-dessus, les trois exercices valent 39 points. On demande
    un barème sur 30 points, les notes de chaque exercice seront donc
    recalculées de manière à fournir un total qui fait 30 points.

    Par défaut les étudiants commencent avec la note maximale attribuée à
    l'exercice (ce comportement est défini lors de la création du sujet, selon
    qu'on choisit /max/ ou /min/ pour la note initiale). Il est possible de
    modifier ce comportement ponctuellement en utilisant le champ bonus/malus.
    Si la note initiale est la note maximale, on peut avoir une note initiale
    plus faible en mettant un malus (une note négative) à un ou plusieurs
    exercices.  On peut avoir le comportement inverse en mettant un bonus,
    lorsque la note initiale est la minimale.

    Dans l'exemple ci-dessus, le dernier exercice pourra être vu comme un
    exercice bonus (puisque tout le monde commence avec zéro sur cet
    exercice).  Dans une telle situation, il peut être judicieux de ne pas
    demander à ajuster les notes des exercices.

**** Affichage du barème et gestion                                     :dev:
     L'affichage du barème est géré dans le fichier
     [[file:../../src/Alecsia/AnnotationBundle/Resources/views/Dashboard/Sujet/manageExercicesForm.html.twig][manageExercicesForm.html.twig]] et le formulaire est envoyé au
     =DefaultController= de =Sujet= (méthode =gererExercices=).
*** Ajout des rendus
    Une fois le sujet de TP créé on peut charger les rendus des étudiants, en
    cliquant sur le bouton /Nouveau rendu/.  Cela ouvre une boîte modale
    permettant de charger un fichier, qui peut être soit une archive PROF ou
    (de manière plus générale) une archive d'archives ; soit une archive
    contenant un rendu unique, dans le cas où un étudiant n'a pas pu rendre
    son TP dans les conditions habituelles.

    Une fois le chargement réalisé, une archive donnée est attribuée
    automatiquement aux étudiants dont le nom figure dans le nom de
    l'archive. 

    Pour que les rendus des étudiants leur soient automatiquement (et
    correctement !) affectés, il est nécessaire que les étudiants aient séparé
    leurs logins (ou noms de famille) par des =_=. Il est plus sûr d'utiliser
    les logins, car la reconnaissance automatique n'utilise pas les prénoms.
    Les logins ne sont pas les logins du FIL mais les *logins Lille 1*.  Un
    exemple de fichier bien nommé serait =TP1_elie.eiffel_line.ria.tar.gz=, où
    les logins sont =elie.eiffel= et =line.ria=. Un mauvais exemple serait
    =TP1-elie.eiffel.line.ria.targz=. Dans ce cas, la reconnaissance par login
    n'est plus possible. La reconnaissance par nom de famille pourrait
    fonctionner *à condition que les étudiants aient bien renseigné leur nom de
    famille dans l'interface Alecsia*.  Alors il est possible que les étudiants
    soient correctement attribués mais le risque d'erreur est plus grand: en
    découpant le nom de fichier sur les points on retrouvera certes les noms
    de famille de nos étudiants, mais on aura aussi d'autres résultats, qui
    pourraient poser problème. Il vaut mieux tout séparer par des =_=.  En cas
    d'erreur (étudiant mal attribué ou aucun étudiant attribué à une archive),
    il sera possible d'ajouter ou supprimer des étudiants d'un rendu par la
    suite.

    [[file:images/sujet_rendus.jpg]]

    Dans l'exemple ci-dessus, les étudiants ayant réalisé le premier et
    le dernier rendu ont été automatiquement reconnus car leurs logins sont
    présents dans le nom de l'archive. La reconnaissance automatique a échoué
    sur le deuxième rendu car l'étudiant n'est pas encore inscrit.

    Lorsque un (ou plusieurs) rendu est sélectionné, d'autres boutons apparaissent dans l'interface. Ces boutons sont :
    - Marquer comme :: permet de spécifier que la correction d'un ou plusieurs TP est achevée ou, à l'inverse, de définir que la correction d'un TP n'est pas terminée.
                       Lorsque la correction d'un TP donné est marquée comme terminée, la note apparaît sur la droite, à la place du -.
    - Supprimer :: qui permet de supprimer définitivement le rendu.

    L'ensemble des rendus ayant été chargés, il est maintenant temps de
    s'atteler à la correction. 
**** Import d'une archive                                               :dev:
     Géré dans la classe =FileImporter= du fichier
     =../src/Alecsia/AnnotationBundle/Controller/Sujet/FileImporter.php=.
**** Attribution automatique du rendu                                   :dev:
     Géré par la fonction =findByWork= du fichier =StudentRepository.php=.
*** Correction d'un rendu
    Cliquer sur un rendu nous amène dans l'interface de correction de ce rendu.

    [[file:images/corrige.jpg]]

    Intéressons-nous d'abord aux menus présents en haut de l'écran
**** Note
     Le menu /Note/ permet d'afficher le détail de la note : la note aux différents exercices ainsi que les bonus ou malus.

     En outre, cliquer sur un exercice permet de le considérer comme non fait,
     en générant automatiquement une annotation dans ce sens. Un nouveau clic
     permet d'annuler cette annotation.
***** Menu de note                                                      :dev:
      L'affichage est réalisée dans le template [[file:~/Enseignement/PJI/2011--2012/alecsia/lille1/alecsia/src/Alecsia/AnnotationBundle/Resources/views/Correction/Bareme/bareme.html.twig][bareme.html.twig]].
***** Finir un exercice                                                 :dev:
      Le code est géré par la fonction =activateExercise= se trouvant dans le
      fichier ~ExercisesGrades.js~.
**** Réglages
#<<gestion_etudiants_rendu>>
     - Gestion des fichiers :: Liste l'ensemble des fichiers présents dans
          l'archive. Permet d'afficher ou de cacher certains fichiers.  Par
          défaut tous les fichiers ne sont pas affichés. Seuls les fichiers
          non binaires ne dépassant pas une certaine taille sont affichés.
          Sont également exclus de l'affichage tous les fichiers qui
          correspondent à une expression rationnelle entrée dans les [[rgl_import][règles
          d'importation]].
     - Gestion des étudiants :: Permet de modifier les étudiants ayant
          participé au rendu. La modification des étudiants a pour but
          d'ajouter des étudiants ayant participé au TP et dont le nom n'a pas
          été détecté automatiquement ou alors de supprimer des étudiants
          ayant été détectés à tort.  L'interface présente une liste des
          étudiants inscrits au groupe. Sont cochés les noms des étudiants
          affectés aux rendus. Sont grisés les noms des étudiants affectés à
          d'autres rendus de ce sujet.
     - Étudiants non inscrits :: Lorsque des étudiants qui n'ont pas encore un
          compte validé à l'université rende un TP, on peut souhaiter leur
          affecter malgré tout le TP. Cette option permet de le faire, en
          précisant les nom, prénom et email de la personne concernée.  Par la
          suite, lorsque cette personne aura un compte à l'université il sera
          possible de fusionner ces deux comptes en utilisant l'option «
          Étudiants non inscrits » présente dans le menu de gestion du groupe
          (sur la page centralisant les UE).
***** Exclusion de fichiers                                             :dev:
      Certains fichier ne seront pas affichés par défaut dans l'interface.
      La détermination des fichiers dont il s'agit se fait grâce au service =FileGuesser=.

**** Annotation
     Venons-en enfin au cœur de l'application : l'annotation des TP.
     Sélectionner une partie du code source permettra d'ajouter une remarque en lien avec cette portion de code source.
     Une fois la portion sélectionnée, on peut soit cliquer sur le bouton /Annoter/ toujours présent en haut de la page ou alors presser la touche 'a'.
     Dans ces deux cas, une fenêtre modale s'ouvrira.

     [[file:images/corrige_annotation.jpg]]

     La fenêtre propose une liste des modèles déjà existants qu'il est donc
     possible de réutiliser. Il est possible de filtrer les entrées dans cette
     fenêtre en tapant un mot ou le début d'un mot.  Une fois l'annotation qui
     nous intéresse sélectionnée, on a encore la possibilité de la modifier
     pour l'adapter à notre besoin.  Si aucune annotation ne convient il est
     possible d'en créer une nouvelle. Pour cela, il faut aller tout en bas de
     la fenêtre modale ou alors, appuyer sur les touches =:n= ce qui permettra
     de ne conserver que les entrées proposant de créer une nouvelle
     annotation. On peut utiliser les touches du clavier pour sélectionner le
     choix qui convient :
     - Nouvelle annotation :: Nouvelle remarque qui ne pourra pas être
          réutilisée dans ce TP ou dans le TP d'autres étudiants. On suppose
          donc que cette remarque est très spécifique et n'aurait pas
          d'intérêt pour d'autres étudiants.
     - Nouveau modèle dans ... :: Une entrée de cette forme existe en deux exemplaires, une pour le sujet de TP, une autre pour l'UE.
          La nouvelle remarque pourra alors être réutilisée
          pour d'autres étudiants mais uniquement dans le cadre de ce TP ou dans le cadre de l'UE (selon le choix qui a été fait).
     - Nouveau modèle global :: Nouvelle remarque d'ordre général qui pourra
          être réutilisée dans n'importe quel TP, dans n'importe quelle
          UE.
     Une fois choisi le type d'annotations à ajouter, il s'agit de remplir les
     détails de cette annotation.  La façon dont cette fenêtre doit être
     remplie est expliquée dans la partie sur l'[[add_modif_modele][ajout ou la modification de
     modèles]].

     On peut aussi ajouter une annotation sans qu'elle ne soit reliée à une
     portion du code source. Dans ce cas il s'agit d'une remarque d'ordre
     général.  Il est toujours possible, à n'importe quel moment d'appuyer sur
     la touche =a=, ou cliquer sur le bouton /Annoter/ pour ajouter une
     annotation.  Si rien n'est sélectionné dans le code source, ce sera
     considéré comme une remarque générale qui se placera alors tout en haut
     du rendu.

     Une fois une annotation ajoutée, la note sera automatiquement mise à jour
     en conséquence. Une note ne peut jamais devenir négative (que ce
     soit la note d'un exercice ou la note globale d'un TP).

     Les annotations peuvent être modifiées /a posteriori/ y compris si elles
     dépendent d'un modèle. Dans ce dernier cas, la modification de
     l'annotation pourra impacter, si l'utilisateur le désire, le modèle et
     donc l'ensemble des annotations fondées sur ce modèle.  C'est pourquoi
     après avoir modifié une telle annotation, Alecsia demandera si la
     modification doit concerner seulement cette annotation ou le modèle (et
     donc l'ensemble des annotations construites sur ce modèle).  Il est donc
     important de *bien réfléchir* avant de modifier tout un modèle puisque
     cela peut avoir un impact sur un grand nombre de TP !  À noter toutefois
     que toute modification réalisée dans une annotation indépendamment du
     modèle, sera conservée en dépit d'une modification du modèle. Autrement
     dit une modification dans l'annotation est plus forte qu'une modificaiton
     dans le modèle.

***** Gestion des annotations et modèles                  :dev:
      *Attention* la partie du code sur la gestion des annotations et modèles
      nécessite certainement un gros factoring en raison des doublons entre
      les deux.
****** Affichage
       Le fichier de template pour les annotations se trouve dans
       [[file:../../src/Alecsia/AnnotationBundle/Resources/views/Correction/Fichier/annotation.html.twig][annotation.html.twig]] et pour les modèles dans [[file:../../src/Alecsia/AnnotationBundle/Resources/views/Dashboard/ListeModeles/modele.html.twig][modele.html.twig]].

       L'affichage utilise un parser Markdown (=parsedown=) pour lequel une
       extension Twig a été mise en place dans [[file:../../src/Alecsia/AnnotationBundle/Twig/Extension/MD2Html.php][MD2Html.php]] à partir d'un
       service =Markdown= ([[file:../../src/Alecsia/AnnotationBundle/Service/Markdown.php][Markdown.php]]) recourant à =Parsedown=.
****** Modification
       La modification des annotations ou modèles se fait dans un premier
       temps via les fichiers javascript [[file:../../src/Alecsia/AnnotationBundle/Resources/public/js/Correction/AnnotationFormManager.js][AnnotationFormManager.js]] et
       [[file:../../src/Alecsia/AnnotationBundle/Resources/public/js/Dashboard/ModeleFormManager.js][ModeleFormManager.js]] qui permettent de créer la fenêtre modale
       contenant l'annotation ou le modèle à modifier.
       
       Le formulaire de modification devant avoir accès au code markdown brut
       (et non interprété par le parser), il est nécessaire de stocker la
       chaîne brute dans la page web (même si elle n'est pas affichée).  Dans
       une annotation ou un commentaire les balises dont les classes sont
       =raw_comment= ou =raw_title= contiennent les versions brutes (non
       interprétées) qui sont celles devant apparaître dans le formulaire.

       La modification d'une annotation est ensuite gérée par le contrôleur
       =GererAnnotationsController= ([[file:../../src/Alecsia/AnnotationBundle/Controller/Correction/GererAnnotationsController.php][GererAnnotationsController.php]]) et celle
       d'un modèle sont gérée dans le contrôleur =GererModelesController=
       ([[file:../../src/Alecsia/AnnotationBundle/Controller/Sujet/GererModelesController.php][GererModelesController.php]]).

*** Ajout ou modification de modèles
#<<add_modif_modele>>
    Avant de commencer la correction, il est possible d'anticiper un certain
    nombre d'erreurs que les étudiants ne manqueront pas de faire. Afin de
    mettre en évidence ces erreurs, et pour éviter d'avoir à retaper à chaque
    fois le même message, on peut choisir de créer des modèles.

    Ces modèles, sont des remarques faites aux étudiants qui leur explique en
    quoi leur code n'est pas correct ou n'est pas idéal.  On peut également
    associer une pénalité (ou un bonus) à un modèle afin d'impacter la note du
    TP.
    
    Pour créer ou modifier un modèle, il faut cliquer sur l'onglet modèles.
    Dans cet onglet sont affichés tous les modèles propres à ce TP, s'il en existe.

    [[file:images/sujet_rendus_modeles.jpg]]

    Il est alors possible de modifier un modèle existant ou créer un autre
    modèle, en cliquant sur /Nouveau modèle/.

    Dans les deux cas, une fenêtre modale s'ouvrira.

    [[file:images/sujet_rendus_modeles_nouveau.jpg]]

    Cette fenêtre permet de rentrer une nouvelle annotation qui pourra être
    reprise dans tout le TP. Cette annotation doit disposer d'un titre
    (obligatoire) ; d'une valeur, c'est-à-dire le nombre de points à retirer
    ou à ajouter lorsque cette annotation est mise ; d'un commentaire
    (potentiellement formatté en Markdown)
    expliquant ce qui va ou ce qui ne va pas dans la portion de code concernée
    et enfin il faut préciser l'exercice auquel se rapporte cette annotation,
    ou bien sélectionner /Bonus/Malus/ si jamais il s'agit d'une remarque
    générale qui n'est pas spécifique à un exercice.

    La valeur rentrée peut soit être absolue (par exemple $0,5$ pour ajouter
    0,5 point, ou $-2$ pour en retirer 2) soit être relative ($25\,\%$ ou
    $-10\,\%$) et dans ce cas la note est relative au nombre de points de
    l'exercice.  Pour un exercice sur 2 points, $25\,\%$ ajoutera $0,5$ point
    alors que $-10\,\%$ en retirera $0,2$.

    Lors de la modification d'un modèle, tout changement dans le modèle
    impactera les rendus auxquels une annotation utilisant ce même modèle a
    été déposée. Par exemple, si je passe la pénalité d'un modèle de $-1$ à
    $-2$, les notes des TP possédant ce modèle d'annotation seront mises à
    jour, y compris pour les TP passés, à moins que la note n'ait été /gelée/
    ou sauf si, dans un TP particulier, la note de l'annotation a été modifiée
    manuellement.

    Dans cette fenêtre la validation du formulaire se fait en appuyant sur le
    bouton situé en bas à droite, ou alors en appuyant sur les touches Ctrl+s
    si on est en train d'éditer un champ.
    
*** Consultation du rendu par les étudiants
    Une fois les TP corrigés, il est possible de les rendre consultable aux
    étudiants (dans le menu Gérer du sujet) afin qu'ils puissent voir les
    annotations qui ont été déposées. 

    Les étudiants sont informés de l'état d'avancement de la correction.

    [[file:images/consultation_etudiants.jpg]]

    Dans l'exemple ci-dessus, le TP1 n'a pas encore été corrigé (l'enseignant
    n'a pas marqué que la correction était terminée pour le rendu de cet
    étudiant). 
    Le TP2 a été corrigé, le rendu marqué comme terminé et le sujet
    a été rendu consultable. L'étudiant peut donc cliquer sur le TP2 pour voir
    les annotations mises par son enseignant. La vue à laquelle il accède est
    la même que celle de l'enseignant lorsqu'il corrige le TP… hormis qu'elle
    est en lecture seule.
    Le TP3 a été corrigé (le rendu a été marqué comme terminé), mais le sujet n'a 
    pas encore été rendu consultable par l'enseignant.
    
* Configuration
** Règles d'importation 
#<<rgl_import>>
   Les règles d'importation définissent:
   1. les fichiers qui seront effectivement affichés dans l'interface de correction (tous les fichiers sont conservés) ;
   2. les noms de dossiers à exclure pour l'importation ;
   3. les liens à faire entre langage et extensions des fichiers.

   Chacune des règles est définie en utilisant une expression rationnelle.
