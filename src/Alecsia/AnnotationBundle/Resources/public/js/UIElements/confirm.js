
/* Affichage des messages d'erreur */
Confirm = new Object();
Confirm.show = function (titre, message, callback, boutonlabel) {

   $confirmbox = $("#modal_confirmation");
   $title = $("#modal_confirmation").find(".header > h3");
   $message = $("#modal_confirmation").find("p");
   $okbutton = $("#confirm_ok");

   if (boutonlabel) {
      $okbutton.text(boutonlabel);
   } else {
      $okbutton.text("OK");
   }

   $title.html(titre);
   $message.html(message);

   $okbutton.unbind("click");
   $okbutton.click(callback);

   $confirmbox.jqmShow();
};

Confirm.rebindButton = function ($button) {
   if (typeof ($button.data("events")) != "undefined") {

      // Désassociation des handlers
      var clickHandlers = $button.data("events").click.slice();
      $button.unbind("click");

      // Infos confirm box
      var titre = $button.attr("data-confirm-h");
      var paragraphe = $button.attr("data-confirm-p");
      var label = $button.attr("data-confirm-l");

      // Reassociation
      for (var i = 0; i < clickHandlers.length; i++) {
         var handler = clickHandlers[i].handler;
         $button.bind("clickConfirmed", handler);
      }

      $button.click(function (e) {
         Confirm.show(titre, paragraphe, (function ($bouton) {
            return function () {
               $bouton.trigger("clickConfirmed");
            }
         })($button), label);
         e.preventDefault();
      });
   }
}

Confirm.rebindLink = function ($link) {

   // Infos confirm box
   var titre = $link.attr("data-confirm-h");
   var paragraphe = $link.attr("data-confirm-p");
   var label = $link.attr("data-confirm-l");
   var href = $link.attr("href");

   $link.click(function (e) {
      Confirm.show(titre, paragraphe, (function (href) {
         return function () {
            location.replace(href);
         }
      })(href), label);
      e.preventDefault();
   });
}


$(function () {
   $("#modal_confirmation").modal();

   // Rebind des elements à confirmer
   $("button[data-confirm-h]").each(function () {
      Confirm.rebindButton($(this));
   });
   $("a[data-confirm-h]").each(function () {
      Confirm.rebindLink($(this));
   });
});
