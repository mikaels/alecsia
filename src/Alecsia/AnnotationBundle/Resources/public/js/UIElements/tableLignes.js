// tableLignes.js
// Des tableaux de lignes (dés)activables en JS.

$(function () {

   // Fonctions utiles
   tableLignes = {
      // renvoie [nbtotal,nbactifs,nbinactifs]
      getInfos: function ($table) {
         var $lignes = $table.find("tbody tr");
         var nbtotal = $lignes.size();
         var nbactifs = $lignes.filter("tr.actif").size();
         var nbinactifs = $lignes.filter("tr.inactif").size();

         return {
            nbtotal: nbtotal,
            nbactifs: nbactifs,
            nbinactifs: nbinactifs
         };
      },
      activerLigne: function ($tr) {
         $tr.removeClass("inactif").addClass("actif");
         $tr.find("input:not(.activator),select,textarea").removeAttr("disabled");
         $tr.find("input.activator").attr("checked", "checked");

         // Events
         var $table = $tr.parent().parent();
         var infos = tableLignes.getInfos($table);

         $table.trigger("selection");
         if (infos.nbinactifs == 0) {
            $table.trigger("selectiontotal");
         }
      },
      desactiverLigne: function ($tr) {
         $tr.removeClass("actif").addClass("inactif");
         $tr.find("input:visible:not(.activator),select,textarea").attr("disabled", "disabled");
         $tr.find("input.activator").removeAttr("checked");

         // Events
         var $table = $tr.parent().parent();
         var infos = tableLignes.getInfos($table);

         $table.trigger("deselection");
         if (infos.nbactifs == 0) {
            $table.trigger("deselectiontotal");
         }
      },
      activerTout: function ($table) {
         $table.children("tbody").children("tr").each(function () {
            tableLignes.activerLigne($(this));
         });
      },
      desactiverTout: function ($table) {
         $table.children("tbody").children("tr").each(function () {
            tableLignes.desactiverLigne($(this));
         });
      },
      init: function ($table) {

         $table.find(".activator").change(function () {
            $tr = $(this).parents(".tableLignes tr, .tableSelect tr");
            $table = $tr.parents(".tableLignes, .tableSelect");
            if (this.checked) {
               tableLignes.activerLigne($tr);
            } else {
               tableLignes.desactiverLigne($tr);
            }
         });

      }

   }


   // Bind evenement checkbox
   $(".tableLignes, .tableSelect").each(
           function () {
              tableLignes.init($(this));
           }

   );


});
