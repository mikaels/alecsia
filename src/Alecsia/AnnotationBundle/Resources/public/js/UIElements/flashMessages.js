
/* Fonctions utiles pour la gestion des Flash Messages */
FlashMessages = new Object();
FlashMessages.animateFlashes = function () {

   // Success Flashes
   $flashes = $(".flash.success");
   $flashes.each(function () {
      $(this).removeClass("lookatme", 1000, function () {
         $(this).fadeOut(500);
      });
   })
};

/* On anime d'entrée de jeu les flash messages */
$(function () {
   FlashMessages.animateFlashes();
})
