// Permet d'affecter le même comportement d'ouverture/fermeture à toutes les boites modales

function showModalDialog(hash) {
   var $window = hash.w;

   original_scrolltop = $(document).scrollTop();

   if (hash.c.noanimate) {
      $window.css("top", "0");
      $window.css("display", "block");
      $window.find('.initfocus:visible').eq(0).focus();
   }
   else {
      // On la positionne d'abord trop haut.
      $window.css("display", "block");

      var window_h = $window.outerHeight();
      $window.css("top", -window_h);

      // On la descend progressivement
      $window.animate({top: 0}, 200, function () {
         $(this).find('.initfocus:visible').eq(0).focus();
      });
   }

   DDLink.prototype.hideAllPanel();
}

function hideModalDialog(hash) {
   var $window = hash.w;

   var window_h = $window.outerHeight();
   $(".jqmOverlay").eq(0).remove();

   if (hash.c.noanimate) {
      $window.css("display", "none");
      getTopModal().find('.initfocus:visible').eq(0).focus();
   } else {
      $window.animate({top: -window_h}, 200, 'linear', function () {
         $(this).css('display', 'none');
         getTopModal().find('.initfocus:visible').eq(0).focus();
      });
   }

   // Nettoyage des formulaires
   $window.find("form").each(function () {
      this.reset();
      $(this).find("input[type=checkbox]").change();
   });

   $window.find("input").not("form input").val("");

   $window.find("*").blur();

   // On deselectionne le texte
   $("body").mouseup();
}

function getTopModal() {
   var zindex = -1;
   var res = $();
   var $modals = $(".jqmWindow:visible,.jqmSuperModal:visible");

   for (var i = 0; i < $modals.size(); i++) {
      var $modal = $modals.eq(i);
      var modal_zindex = parseInt($modal.css("z-index"));
      if (modal_zindex > zindex) {
         zindex = modal_zindex;
         res = $modal;
      }
   }

   return res;

}

$(function () {

   $.fn.modal = function (options) {
      if (!options) {
         options = new Object();
      }
      options.onShow = showModalDialog;
      options.onHide = hideModalDialog;
      this.jqm(options);
   };

   // Reactivité à la touche Echap
   $(document).keyup(function (e) {
      if (e.which == 27) {
         getTopModal().jqmHide();
      }
   });

   $(function () {
      // Auto-assignement des modales
      $(".jqmWindow,.jqmSuperModal").each(function () {
         var $this = $(this);
         var id = $this.attr("id");

         var options = new Object();
         options.trigger = ".go_" + id;

         if ($this.hasClass("noanimate")) {
            options.noanimate = true;
         }

         $this.modal(options);
      });

      // Installation Go and Assign
      $(".go_and_assign").click(function () {
         var $this = $(this);
         var assign = $.parseJSON($(this).attr("data-assign"));

         for (id in assign) {

            var value = assign[id];

            // Changement classe
            if (id.substr(0, 12) == "_addclassto_") {
               var realid = id.substr(12);
               var $elt = $("#" + realid);
               $elt.addClass(value);
            }

            else if (id.substr(0, 12) == "_remclassto_") {
               var realid = id.substr(12);
               var $elt = $("#" + realid);
               $elt.removeClass(value);
            }

            // Changement href 
            else if (id.substr(0, 6) == "_href_") {
               var realid = id.substr(6);
               var $elt = $("#" + realid);
               $elt.attr("href", value);
            }

            // Changement valeur / texte
            else {
               var $elt = $("#" + id);
               if ($elt.filter(":input").size() > 0) {
                  $elt.val(value);
               } else {
                  $elt.html(value);
               }
            }
         }
      });
   });
});

