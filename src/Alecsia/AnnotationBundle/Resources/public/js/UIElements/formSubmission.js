$("form[asynchronous-submission]").submit(function () {
   $.post($(this).attr('action'), $(this).serialize(), function (res) {
      if (res != null && res.length > 0) {
         res = $.parseJSON(res);
         if (res.notification != null) {
            if (res.notification.erreur) {
               Errors.show(res.notification.titre, res.notification.contenu);
            } else {
               Notice.show(res.notification.titre, res.notification.contenu);
            }
         }
      }
   });
   getTopModal().hide();
   return false; // prevent default action
});
