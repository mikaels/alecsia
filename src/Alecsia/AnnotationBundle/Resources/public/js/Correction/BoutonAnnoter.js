// Clic sur bouton Annoter
$(function () {
   $("#bouton_annoter").click(function () {
      AnnotationFormManager.get.initFormAjout();
      $("#modal_add_annotation").jqmShow();
   });
});

// Association à la touche 'a'
$(function () {
   $(document).keydown(function (e) {
      if ($(".jqmWindow:visible").size() == 0) {
         if (e.which == 65) {
            $("#bouton_annoter").addClass("active");
         }
      }
   });

   $(document).keyup(function (e) {
      if ($(".jqmWindow:visible").size() == 0) {
         if (e.which == 65) {
            $("#bouton_annoter").removeClass("active");
            $("#bouton_annoter").click();
         }
      }
   })
});

// Evenements selection
$("body").mouseup(
        function () {
           window.setTimeout(function () {
              var sel = SelectionManager.getSelectionOffsets();
              if (sel != null) {
                 var nom_fichier = $("#f_" + sel[0]).find(".header > h3").text();
                 $(document).trigger("fileSelected", [nom_fichier]);
              } else {
                 $(document).trigger("fileUnselected");
              }

           }, 20);
        }
);

// Changement du label
$(document).bind("fileSelected", function (e, filename) {
   var label = "Annoter <em>" + filename + "</em>";
   $("#addannot_header").html(label);
   $("#bouton_annoter").html(label);
});

$(document).bind("fileUnselected", function (e) {
   var label = "Annoter le rendu";
   $("#addannot_header").html(label);
   $("#bouton_annoter").html(label);
});