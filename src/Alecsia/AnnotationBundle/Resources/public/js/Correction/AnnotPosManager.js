// Objet chargé de placer les annotations de façon à ce qu'elles soient le plus proche possible
// de la ligne à laquelle elles font référence.

function AnnotPosManager(id_annotpanel, id_cp) {

   this.$ol = $("#cp_" + id_cp).children().children("ol");
   this.$annots = $("#ap_" + id_annotpanel);

   this.nblignes = this.$ol.children("li").size();
   this.encombrements = new Array();

   if (typeof AnnotPosManager.initialized == "undefined") {



      /*==========================
       = Récupération des infos =
       ========================== */
      // Renvoie [annot_id,offset_y,height], à savoir des infos de placement sur
      // l'annotation d'id passé en paramètre
      AnnotPosManager.prototype.getEncombrement = function (annot_id) {
         var $annotation = this.$annots.children("#an_" + annot_id);
         annot_id = parseInt(annot_id);

         if ($annotation.size() == 1) {
            var num_ligne = parseInt($annotation.find(".infos_annot > .dl").text());
            var pos_y = this.$ol.children("li").eq(num_ligne - 1).position().top;
            var marge_haute = this.$ol.offsetParent().position().top;
            var offset_y = pos_y + marge_haute;
            var height = $annotation.outerHeight(true);
            return {
               annot_id: annot_id,
               offset_y: offset_y,
               height: height
            };
         }

         else {
            return null;
         }
      };


      // Utilitaire servant à trier les encombrements entre eux
      AnnotPosManager.sortEncombre = function (a, b) {

         if (a.offset_y > b.offset_y) {
            return 1;
         }
         else if (a.offset_y < b.offset_y) {
            return -1;
         }
         else {
            if (a.height > b.height) {
               return 1;
            }
            else if (a.height < b.height) {
               return -1;
            }
            else {
               return 0;
            }
         }
      };

      // Recalcule la table des {annot_id,ligne,height} destinée à être traitée en entrée
      AnnotPosManager.prototype.refreshTableEncombrement = function () {

         this.encombrements = new Array();

         // Exploration de chaque annotation du panel associé au manager
         this.$annots.children("li").each(function (manager) {

            // Ajout d'un array d'encombrement
            function ajouterEncombrement() {
               $this = $(this);
               var annot_id = $this.attr("id").substr(3);
               var encombre = manager.getEncombrement(annot_id);
               manager.encombrements.push(encombre);
            }

            return ajouterEncombrement;
         }(this));

         // Tri des nouveaux éléments
         this.encombrements = this.encombrements.sort(AnnotPosManager.sortEncombre);
         return this.encombrements;
      };

      /*========================
       = Traitement des infos =
       ======================== */
      // A partir des infos données dans this.encombrements, calcule la position conseillée
      // des annotations. Calcule aussi la nouvelle taille à adopter pour la liste d'annotations.
      AnnotPosManager.prototype.calculerPositionsGlouton = function () {
         var seuil_y = 0;
         var positions = new Array();

         for (i = 0; i < this.encombrements.length; i++) {
            var encombrement = this.encombrements[i];
            var height = encombrement.height;
            var annot_id = encombrement.annot_id;

            // Calcul de l'offset 
            var offset_y = encombrement.offset_y;

            if (offset_y < seuil_y) {
               offset_y = seuil_y;
            }

            // Mise à jour du seuil
            seuil_y = offset_y + height;

            var position_annot = {
               annot_id: annot_id,
               pos_y: offset_y
            };

            positions.push(position_annot);
         }

         return {
            positions: positions,
            new_height: seuil_y
         };
      };

      /*=============================
       = Application du traitement =
       ============================= */
      AnnotPosManager.prototype.repositionner = function () {
         this.refreshTableEncombrement();
         var resultat = this.calculerPositionsGlouton();
         var positions = resultat.positions;
         var new_height = resultat.new_height;

         // Positionnement des annotations
         for (i in positions) {
            var position = positions[i];
            var annot_id = "an_" + position.annot_id;
            $("#" + annot_id).css("top", position.pos_y);
         }

         // Application de la nouvelle taille
         this.$annots.css("height", new_height);
      };


      /*==================
       = Initialisation =
       ================== */
      AnnotPosManager.prototype.init = function (id_annotpanel, id_cp) {
         AnnotPosManager[id_annotpanel] = this;
      };

      AnnotPosManager.initialized = true;
   }

   this.init(id_annotpanel, id_cp);
}



// Construction auto des APManager
function installAPos() {
   $(".fichier").each(function () {
      var $this = $(this);
      var $alist = $this.find(".annotation_panel > ul");

      if ($alist.size() > 0) {
         var id_annotpanel = parseInt($alist.attr("id").substr(3));

         var ap = new AnnotPosManager(id_annotpanel, id_annotpanel);

      }
   });
}

$(installAPos);