function AnnotationEnTeteList() {

   this.$list = $("ul#ap_0");
   this.$widget = $("#ap_entete");

   if (typeof AnnotationEnTeteList.initialized == "undefined") {

      /*=====================================
       = Ajout / Suppression d'annotations =
       ===================================== */

      /* Ajoute une annotation grâce à son code HTML. */
      AnnotationEnTeteList.prototype.addAnnotation = function (html) {
         $html = $(html);

         // Vérification de la validité de l'annotation :
         // 1 - Nombre d'annotations = 1
         var nbAnnotDansHtml = $html.filter("li.annot").size();
         if (nbAnnotDansHtml != 1) {
            return false;
         }

         // 2 - Annotation non existante.
         var idAnnot = $html.attr("id");
         var nbAnnotAvecMemeId = $("#" + idAnnot).size();
         if (nbAnnotAvecMemeId != 0) {
            return false;
         }


         // Ajout d'une annotation
         var ajoutEffectif = function (liste, html) {
            return function () {
               liste.append(html);
               AnnotationEnTeteList.eventify(html);
               html.removeClass("lookatme", 1000);
            }
         }(this.$list, $html);

         // On fait apparaitre la liste en cas de besoin
         if (this.$widget.hasClass("hidden")) {
            this.displayList(ajoutEffectif);
         } else {
            ajoutEffectif();
         }

      };

      /* Modifier une annotation grâce au code HTML de cette dernière, modifiée. */
      AnnotationEnTeteList.prototype.updateAnnotation = function (html) {
         $newannot = $(html);

         // Vérification de la validité de l'annotation :
         // 1 - Nombre d'annotations = 1
         var nbAnnotDansHtml = $newannot.filter("li.annot").size();
         if (nbAnnotDansHtml != 1) {
            return false;
         }

         // 2 - Annotation existante?
         var idAnnot = $newannot.attr("id");
         var $annotAModifier = $("#" + idAnnot);
         if ($annotAModifier.size() == 0) {
            return false;
         }

         // Modification de l'annotation
         $annotAModifier.html($newannot.html());

         // Animation
         $annotAModifier.addClass("lookatme");
         $annotAModifier.removeClass("lookatme", 1000);
      };

      /* Supprimer une annotation */
      AnnotationEnTeteList.prototype.deleteAnnotation = function (id) {
         var annot_id = "an_" + id;
         var $annot = $("#" + annot_id);

         if ($annot.size() != 0) {

            // On fait disparaître la liste si elle n'a plus raison d'être
            if (this.$list.children().size() == 1) {
               this.hideList();
               $annot.remove();
            }
            // Ou uniquement l'annotation.
            else {
               $annot.slideUp(400, "easeOutQuad", function () {
                  var $this = $(this);
                  $this.remove();
               });
            }
         }
      };

      /*====================================
       = Affichage / Masquage de la liste =
       ==================================== */
      AnnotationEnTeteList.prototype.displayList = function ($callback) {
         this.$widget.slideDown(200, "easeOutQuad", $callback);
         this.$widget.removeClass("hidden");
      };

      AnnotationEnTeteList.prototype.hideList = function () {
         this.$widget.slideUp(200, "easeOutQuad", function () {
            $(this).addClass("hidden");
         });
      };

      /* ==================
       = Event Handlers =
       ================== */
      AnnotationEnTeteList.handlers = new Object();

      // Appelé lorsque la souris entre sur une annot.
      AnnotationEnTeteList.handlers.mouseInAnAnnotation = function (evt) {
         $this = $(this);
         $this.addClass("hover");
      };

      // Appelé lorsque la souris sort d'une annot.
      AnnotationEnTeteList.handlers.mouseOutAnAnnotation = function (evt) {
         $this = $(this);
         $this.removeClass("hover");
      };

      // Appelé lors d'un clic sur une annotation
      AnnotationEnTeteList.handlers.clickOnAnAnnotation = function (evt) {
         $this = $(this);

         var idAnnot = $this.attr("id");

         AnnotationFormManager.get.initFormModif(idAnnot);
         $("#modal_add_annotation").jqmShow();
      };


      // Ajout des comportements sur une annotations
      AnnotationEnTeteList.eventify = function ($annot) {
         $annot.hover(
                 AnnotationEnTeteList.handlers.mouseInAnAnnotation,
                 AnnotationEnTeteList.handlers.mouseOutAnAnnotation
                 );

         if (!($("#correctionUI").hasClass("lectureseule"))) {
            $annot.click(
                    AnnotationEnTeteList.handlers.clickOnAnAnnotation
                    );
         }
      };



      /* ==================
       = Initialisation =
       ================== */
      AnnotationEnTeteList.prototype.init = function (id_list) {

         // Référencement des objets AnnotationEnTeteList
         AnnotationEnTeteList.get = this;


         // Association des comportements aux annotations
         this.$list.children(".annot").each(function () {
            $this = $(this);
            AnnotationEnTeteList.eventify($this);
         });

      };


      AnnotationEnTeteList.initialized = true;
   }

   this.init();
}

// Recherche auto des listes à gérer
function installEnTeteM() {
   $("#ap_0").each(function () {
      $this = $(this);
      new AnnotationEnTeteList();
   });
}

$(installEnTeteM);