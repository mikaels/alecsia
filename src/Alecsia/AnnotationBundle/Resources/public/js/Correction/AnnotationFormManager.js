function AnnotationFormManager(id_form) {

   this.$annotation_form = $("#" + id_form);
   this.$window = this.$annotation_form.parents(".jqmWindow");

   this.$bouton_delete = $("#fma_delete");
   this.$bouton_submit = $("#fma_submit");

   // Bouton desactivable
   this.$bouton_desactive = null;
   this.bouton_label = null;

   this.$aid = this.$annotation_form.find("input#fma_annot_id");
   this.$fid = this.$annotation_form.find("input#fma_fichier_id");
   this.$dl = this.$annotation_form.find("input#fma_debutligne");
   this.$dc = this.$annotation_form.find("input#fma_debutcolonne");
   this.$fl = this.$annotation_form.find("input#fma_finligne");
   this.$fc = this.$annotation_form.find("input#fma_fincolonne");
   this.$nom = this.$annotation_form.find("input#fma_nom");
   this.$comm = this.$annotation_form.find("textarea#fma_commentaire");
   this.$ex = this.$annotation_form.find("select#fma_exercice");
   this.$val = this.$annotation_form.find("input#fma_valeur");
   this.$propag = this.$annotation_form.find("input#fma_propager");

   // Mémoire du dernier exercice sélectionné
   this.lastex = 0;

   // Dialogue "propagation"
   this.$ask_propag = $("#modal_ask_propag");

   if (typeof AnnotationFormManager.initialized == "undefined") {

      /*============================
       = Soumission du formulaire =
       ============================ */
      AnnotationFormManager.prototype.submitForm = function () {

         // Préparation des données (valeurs de champs...)
         var URL_controleur = this.$annotation_form.attr("action");

         var data = new Object();
         data["annot_id"] = $("input#fma_annot_id").val();
         data["rendu_id"] = $("input#fma_rendu_id").val();
         data["debutligne"] = $("input#fma_debutligne").val();
         data["debutcolonne"] = $("input#fma_debutcolonne").val();
         data["finligne"] = $("input#fma_finligne").val();
         data["fincolonne"] = $("input#fma_fincolonne").val();
         data["fichier_id"] = $("input#fma_fichier_id").val();
         data["nom"] = $("input#fma_nom").val();
         data["commentaire"] = $("textarea#fma_commentaire").val();
         data["valeur"] = $("input#fma_valeur").val();
         data["exercice"] = $("select#fma_exercice").val();
         data["propager"] = $("input#fma_propager").val();
         data["modele_id"] = $("input#fma_modele_id").val();
         data["makemodelein"] = $("input#fma_makemodelein").val();

         if (data["exercice"] != -1) {
            this.lastex = data["exercice"];
         }

         // Envoi de la requête AJAX au contrôleur
         $.post(URL_controleur, data, AnnotationFormManager.traiterReponse, "json")
                 .error(AnnotationFormManager.traiterErreurServeur);
         this.disable(this.$bouton_submit);

      }

      /*====================
       = Bouton supprimer =
       ==================== */
      AnnotationFormManager.prototype.submitDelete = function () {
         // URL du controlleur
         var URL_controleur = this.$annotation_form.attr("data-delaction");

         // Données
         var data = new Object();
         data["annot_id"] = $("input#fma_annot_id").val();

         // Envoi requête AJAX
         $.post(URL_controleur, data, AnnotationFormManager.traiterReponse, "json")
                 .error(AnnotationFormManager.traiterErreurServeur);
         this.disable(this.$bouton_delete);
      }


      /*==========================
       = Traitement de réponses =
       ========================== */
      AnnotationFormManager.traiterReponse = function (data, textStatus, jqXHR) {
         var reponse = data;

         // traitement des ajouts
         for (var i = 0; i < reponse.ajouts.length; i++) {
            var html = reponse.ajouts[i];
            AnnotationFormManager.traiterReponseAjout(html);
         }

         // traitement des modifs
         for (var i = 0; i < reponse.modifs.length; i++) {
            var html = reponse.modifs[i];
            AnnotationFormManager.traiterReponseModif(html);
         }

         // traitement des suppressions
         for (var i = 0; i < reponse.supprs.length; i++) {
            var id = reponse.supprs[i];
            AnnotationFormManager.traiterReponseSuppression(id);
         }

         // traitement de l'erreur
         if (reponse.notification != null) {
            if (reponse.notification.erreur == 1) {
               Errors.show(reponse.notification.titre, reponse.notification.contenu);
            } else {
               Notice.show(reponse.notification.titre, reponse.notification.contenu);
            }
            AnnotationFormManager.get.enable();
            ModeleChooser.desactiver();
         } else {
            // Disparition de la modale
            $("#modal_add_annotation").jqmHide();
            AnnotationFormManager.get.clean();
         }

         // Refresh de la note
         BaremeManager.refresh();

      }

      // REPONSE RECUE 
      // Ajout :
      AnnotationFormManager.traiterReponseAjout = function (html) {

         // Ajout de l'annotation
         var al_id = $(html).find(".infos_annot > .f_id").text();
         if (al_id != "null") {
            AnnotationList[al_id].addAnnotation(html);
         } else {
            AnnotationEnTeteList.get.addAnnotation(html);
         }

         var exprior = AnnotationFormManager.get.lastex;
         ModeleChooser.recupererModeles(exprior);
      }

      // Modif : 
      AnnotationFormManager.traiterReponseModif = function (html) {

         // Modif de l'annotation
         var al_id = $(html).find(".infos_annot > .f_id").text();
         if (al_id != "null") {
            AnnotationList[al_id].updateAnnotation(html);
         } else {
            AnnotationEnTeteList.get.updateAnnotation(html);
         }

         var exprior = AnnotationFormManager.get.lastex;
         ModeleChooser.recupererModeles(exprior);
      }

      // Suppression : 
      AnnotationFormManager.traiterReponseSuppression = function (an_id) {

         // Suppression de l'annotation
         var al_id = $("#an_" + an_id).find(".infos_annot > .f_id").text();
         if (al_id != "null") {
            AnnotationList[al_id].deleteAnnotation(an_id);
         } else {
            AnnotationEnTeteList.get.deleteAnnotation(an_id);
         }
      }

      // Erreur (du serveur, type 500) 
      AnnotationFormManager.traiterErreurServeur = function () {
         Errors.show("Oh, non...", "Pour une raison inconnue, le serveur a refusé cette action.");
         AnnotationFormManager.get.enable();
         ModeleChooser.desactiver();
      }

      /*=========
       = Etats =
       ========= */
      AnnotationFormManager.prototype.disable = function ($bouton) {
         this.$bouton_desactive = $bouton;
         this.bouton_label = $bouton.text();
         this.$annotation_form.find("input,textarea,select,button").attr("disabled", "disabled");
         $bouton.text("Veuillez patienter...");
      };

      AnnotationFormManager.prototype.enable = function () {
         this.$annotation_form.find("input,textarea,select,button").removeAttr("disabled");
            if (this.$bouton_desactive != null) {
         this.$bouton_desactive.text(this.bouton_label);
            }
      };

      AnnotationFormManager.prototype.clean = function () {
         this.$annotation_form.find("form").each(function () {
            this.reset();
         });
         ModeleChooser.clean();
         this.enable();
      };


      /*===================
       = Bouton "Submit" =
       =================== */
      AnnotationFormManager.submitHandlers = new Object();

      AnnotationFormManager.submitHandlers.justSubmitForm = function (evt) {
         AnnotationFormManager.get.submitForm();
         evt.preventDefault();
      }

      AnnotationFormManager.submitHandlers.warnForPropagation = function (evt) {
         AnnotationFormManager.get.$ask_propag.jqmShow();
         evt.preventDefault();
      }





      /*==============================
       = Construction du formulaire =
       ============================== */
      // AJOUT
      AnnotationFormManager.prototype.initFormAjout = function () {
         // Comportement bouton submit
         this.$annotation_form.unbind("submit");
         this.$annotation_form.submit(AnnotationFormManager.submitHandlers.justSubmitForm);

         // Remplissage champs invisibles
         var sel = SelectionManager.getSelectionOffsets();
         if (sel == null) {
            sel = [null, 0, 0, 0, 0];
         }
         this.$fid.val(sel[0]);
         this.$dl.val(sel[1]);
         this.$dc.val(sel[2]);
         this.$fl.val(sel[3]);
         this.$fc.val(sel[4]);
         this.$aid.val("-1");

         // Champ d'action
         var actionURL = this.$annotation_form.attr("data-addaction");
         this.$annotation_form.attr("action", actionURL);

         // Classe CSS
         this.$window.removeClass("tomodif");
         this.$window.addClass("toadd");

         // Label
         this.bouton_label = "Annoter";
         this.$bouton_submit.text(this.bouton_label);

         // Activation du ModeleChooser
         ModeleChooser.activer();
         this.$ex.removeAttr("disabled");

      };

      // MODIF
      AnnotationFormManager.prototype.initFormModif = function (annot_id) {
         var $annot = $("#" + annot_id);

         // Comportement bouton submit différent si l'annotation est liée
         // à un modèle
         this.$annotation_form.unbind("submit");
         var modele_id = $annot.find(".m_id").text();
         if (modele_id != "null") {
            this.$annotation_form.submit(AnnotationFormManager.submitHandlers.warnForPropagation);
         } else {
            this.$annotation_form.submit(AnnotationFormManager.submitHandlers.justSubmitForm);
         }


         // Récup infos de l'annotation à modifier
         var $annot_infos = $annot.children(".infos_annot");
         var annot_id = annot_id.substr(3);
         var fid = $annot_infos.children(".f_id").text();
         var dl = $annot_infos.children(".dl").text();
         var dc = $annot_infos.children(".dc").text();
         var fl = $annot_infos.children(".fl").text();
         var fc = $annot_infos.children(".fc").text();
         var ex = $annot_infos.children(".ex").text();

         var nom = $annot.find(".raw_title").text();
         var comm = $annot.find(".raw_comment").text();
         var valeur = $annot.find(".valeur").text();


         // Remplissage champs invisibles
         this.$fid.val(fid);
         this.$dl.val(dl);
         this.$dc.val(dc);
         this.$fl.val(fl);
         this.$fc.val(fc);
         this.$aid.val(annot_id);

         // Remplissage champs visibles
         this.$nom.val(nom);
         this.$comm.val(comm);
         this.$val.val(valeur);
         this.$ex.val(ex);



         // Champ d'action
         var actionURL = this.$annotation_form.attr("data-modifaction");
         this.$annotation_form.attr("action", actionURL);

         // Classe CSS
         this.$window.removeClass("toadd");
         this.$window.addClass("tomodif");

         // Label
         this.bouton_label = "Modifier";
         this.$bouton_submit.text(this.bouton_label);

         // Desactiver modele chooser
         ModeleChooser.desactiver();
         this.$ex.removeAttr("disabled");

      };



      /*==================
       = Initialisation =
       ================== */
      AnnotationFormManager.prototype.init = function (id_form) {
         AnnotationFormManager.get = this;
         this.$bouton_delete.click(function () {
            AnnotationFormManager.get.submitDelete();
         });
         save_shortcut = function (e) {
             // Save with Ctrl+s or Ctrl+Enter
            if (e.ctrlKey && (e.keyCode === 83 || e.keyCode == 13)) {
               $("#" + id_form).submit();
               return false;
            }
         };
         this.$comm.keydown(save_shortcut);
         this.$ex.keydown(save_shortcut);
         this.$val.keydown(save_shortcut);
         this.$nom.keydown(save_shortcut);
         this.initAskPropag();
      }

      AnnotationFormManager.prototype.initAskPropag = function () {
         this.$ask_propag.modal();
         $("#propag_no").click(function () {
            AnnotationFormManager.get.$propag.val("0");
            AnnotationFormManager.get.$ask_propag.jqmHide();
            AnnotationFormManager.get.submitForm();
         });

         $("#propag_yes").click(function () {
            AnnotationFormManager.get.$propag.val("1");
            AnnotationFormManager.get.$ask_propag.jqmHide();
            AnnotationFormManager.get.submitForm();
         });
      }

      AnnotationFormManager.initialized = true;
   }

   this.init(id_form);
}


// Recherche auto des forms à gérer
function installAFM() {
   new AnnotationFormManager("form_manage_annotation");
}

$(installAFM);
