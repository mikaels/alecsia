
GelerNote = new Object();

GelerNote.$bouton = $("#gelernotes_bouton");
GelerNote.labeloriginel = $("#gelernotes_bouton").html();
GelerNote.urlcontrolleur = $("#gelernotes_bouton").attr("data-verif");
GelerNote.callbackno = $("#gelernotes_bouton").attr("data-callback");

// Passe en mode loading
GelerNote.loading = function () {
   GelerNote.$bouton.removeClass("error");
   GelerNote.$bouton.addClass("loading");
   GelerNote.$bouton.html("<span>Vérifications...</span>");
   GelerNote.$bouton.unbind("click");
}

// Passe le bouton en mode erreur
GelerNote.error = function () {
   GelerNote.$bouton.removeClass("loading");
   GelerNote.$bouton.addClass("error");

   GelerNote.$bouton.html("Erreur survenue. Réessayer?");
   GelerNote.$bouton.click(GelerNote.click);
}

GelerNote.click = function () {
   // REQUETE AJAX
   $.get(GelerNote.urlcontrolleur, GelerNote.traiterReponse)
           .error(GelerNote.error);

   // Feedback de chargement
   GelerNote.loading();
}

GelerNote.traiterReponse = function (data) {
   GelerNote.$bouton.removeClass("loading");
   GelerNote.$bouton.html(GelerNote.labeloriginel);
   GelerNote.$bouton.click(GelerNote.click);
   if (data == "0") {
      Confirm.show("Rendus non terminés",
              "Il est nécessaire de marquer tous les rendus comme terminés avant de pouvoir geler les notes. Voulez-vous le faire maintenant?",
              function () {
                 location.replace(GelerNote.callbackno);
              },
              "Tout marquer comme terminé");
   } else {
      $("#geler_notes_modal").jqmShow();
   }
}




$(function () {
   $("#gel_methode").change(function () {
      $this = $(this);
      if ($this.val() == "arrondirNot") {
         $("#gel_paliers").addClass("hidden");
      } else {
         $("#gel_paliers").removeClass("hidden");
      }
   });

   $("#geler").change(function () {
      $this = $(this);
      if ($this.attr("checked")) {
         $("#gel_submit").removeAttr("disabled");
      } else {
         $("#gel_submit").attr("disabled", "disabled");
      }
   });

   GelerNote.$bouton.click(GelerNote.click);
});