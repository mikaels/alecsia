var Rendus = {
   remplacerNotes: function (html) {
      var listeNotes = $("#sujet_listenotes");
      $("#sujet_listenotes").html(html);
      var $bouton = $("#marquercomme");
      var $panel = $bouton.next();
      var right = $panel.hasClass("right");


      new DDLink($bouton.get(0), $panel.get(0), right);
      tableLignes.init($("#sel_listerendus"));
      selectionDependant.init($("#sel_listerendus"));
   },
   rafraichirNotes: function () {
      var URLNotes = $("#sujet_listenotes").attr("data-access");
      $.get(URLNotes, Rendus.remplacerNotes, "html")
              .error(Rendus.rafraichirNotes);
   }

};

$(function () {
   $("body").bind("notes_changees", Rendus.rafraichirNotes);
});