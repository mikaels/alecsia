NouveauSujet = new Object();

NouveauSujet.open = function (e) {

   var $this = $(this);

   // Recup des infos
   var istd = parseInt($this.attr("data-td"));
   var ue = $this.attr("data-ue");

   if (istd == NaN) {
       istd = 0;
   }
    
   // Application
   $("#ns_ue_id").val(ue);
   $("#ns_is_td").val(istd);

   if (istd) {
      $("#nouveautp").addClass("hidden");
      $("#nouveautd").removeClass("hidden");
   } else {
      $("#nouveautd").addClass("hidden");
      $("#nouveautp").removeClass("hidden");
   }

   $("#nouveau_sujet").jqmShow();
}

$(function () {
   $(".init_nouveausujet").click(NouveauSujet.open);
});