<?php

namespace Alecsia\AnnotationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Alecsia\AnnotationBundle\Entity\Annotation;

/**
 * Générateur de contenu "d'exemple" pour l'application.
 */
class LoadAnnotations extends AbstractDataFixture implements OrderedFixtureInterface {

   private $data = array(/*

             /* array( */
           /*     "modele"    => null, */
           /*     "fichier"   => "REF_helloworld.c", */
           /*     "exercice"  => null, */
           /*     "rendu"     => "REF_rendu_mccartney", */
           /*     "dl"        => 3, */
           /*     "dc"        => 10,     */
           /*     "fl"        => 3, */
           /*     "fc"        => 14, */
           /*     "nom"       => "Pourquoi void ?", */
           /*     "comm"      => "Je me suis toujours demandé pourquoi les gens mettaient parfois void à la place de 'aucun argument'.", */
           /*     "val"       => "-20%" */
           /* ), */

           /* array( */
           /*     "modele"    => null, */
           /*     "fichier"   => "REF_helloworld.c", */
           /*     "exercice"  => "REF_shello_1", */
           /*     "rendu"     => "REF_rendu_mccartney", */
           /*     "dl"        => 6, */
           /*     "dc"        => 5,     */
           /*     "fl"        => 6, */
           /*     "fc"        => 14, */
           /*     "nom"       => "Et le Exit Success ?", */
           /*     "comm"      => "Il est où?", */
           /*     "val"       => "-0.25" */
           /* ), */

           /* array( */
           /*     "modele"    => null, */
           /*     "fichier"   => "REF_drive.c", */
           /*     "exercice"  => "REF_shello_1", */
           /*     "rendu"     => "REF_rendu_lennon", */
           /*     "dl"        => 1, */
           /*     "dc"        => 1,     */
           /*     "fl"        => 12, */
           /*     "fc"        => 19, */
           /*     "nom"       => "Annotation 3", */
           /*     "comm"      => "Commentaire de l'annotation 3", */
           /*     "val"       => "+0.2" */
           /* ), */

           /* array( */
           /*     "modele"    => "REF_modele1", */
           /*     "fichier"   => "REF_drive.c", */
           /*     "exercice"  => null, */
           /*     "rendu"     => "REF_rendu_lennon", */
           /*     "dl"        => 4, */
           /*     "dc"        => 1,     */
           /*     "fl"        => 12, */
           /*     "fc"        => 19, */
           /*     "nom"       => "Modele !", */
           /*     "comm"      => "Ecrase moi !", */
           /*     "val"       => "-10%" */
           /* ) */
   );

   public function doLoad(ObjectManager $manager) {
      foreach ($this->data as $a) {
         if ($a["fichier"] != null) {
            $a["fichier"] = $manager->merge($this->getReference($a["fichier"]));
         }

         if ($a["exercice"] != null) {
            $a["exercice"] = $manager->merge($this->getReference($a["exercice"]));
         }

         if ($a["modele"] != null) {
            $a["modele"] = $manager->merge($this->getReference($a["modele"]));
         }

         $annotation = new Annotation();
         $annotation->setModele($a["modele"]);
         $annotation->setFichier($a["fichier"]);
         $annotation->setExercice($a["exercice"]);
         $annotation->setRendu($manager->merge($this->getReference($a["rendu"])));
         $annotation->setPosDebut($a["dl"], $a["dc"]);
         $annotation->setPosFin($a["fl"], $a["fc"]);
         $annotation->setNom($a["nom"]);
         $annotation->setCommentaire($a["comm"]);
         $annotation->setValeurLitterale($a["val"]);

         $manager->persist($annotation);
      }

      $manager->flush();
   }

   public function getOrder() {
      return 7; // the order in which fixtures will be loaded
   }

   public function getEnvironments() {
      return array('test');
   }

}
