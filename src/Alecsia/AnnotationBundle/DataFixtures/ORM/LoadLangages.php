<?php

namespace Alecsia\AnnotationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Alecsia\AnnotationBundle\Entity\Langage;

/**
 * Générateur de contenu "d'exemple" pour l'application.
 * Références ajoutées :
 */
class LoadLangages extends AbstractDataFixture implements OrderedFixtureInterface {

   private $data = array(
       array(
           "nom" => "Simple texte",
           "id_geshi" => "text",
           "ref" => "REF_langage_text"
       ),
       array(
           "nom" => "ADA",
           "id_geshi" => "ada",
           "ref" => "REF_langage_ada"
       ),
       array(
           "nom" => "Apache Log",
           "id_geshi" => "apache",
           "ref" => "REF_langage_apache"
       ),
       array(
           "nom" => "Bash",
           "id_geshi" => "bash",
           "ref" => "REF_langage_bash"
       ),
       array(
           "nom" => "C",
           "id_geshi" => "c",
           "ref" => "REF_langage_c"
       ),
       array(
           "nom" => "C++",
           "id_geshi" => "cpp",
           "ref" => "REF_langage_cpp"
       ),
       array(
           "nom" => "CSS",
           "id_geshi" => "css",
           "ref" => "REF_langage_css"
       ),
       array(
           "nom" => "HTML",
           "id_geshi" => "html5",
           "ref" => "REF_langage_html5"
       ),
       array(
           "nom" => "Java",
           "id_geshi" => "java",
           "ref" => "REF_langage_java"
       ),
       array(
           "nom" => "Javascript",
           "id_geshi" => "javascript",
           "ref" => "REF_langage_javascript"
       ),
       array(
           "nom" => "Lisp",
           "id_geshi" => "lisp",
           "ref" => "REF_langage_lisp"
       ),
       array(
           "nom" => "Lua",
           "id_geshi" => "lua",
           "ref" => "REF_langage_lua"
       ),
       array(
           "nom" => "Objective C",
           "id_geshi" => "objc",
           "ref" => "REF_langage_objc"
       ),
       array(
           "nom" => "OCaml",
           "id_geshi" => "ocaml",
           "ref" => "REF_langage_ocaml"
       ),
       array(
           "nom" => "Pascal",
           "id_geshi" => "pascal",
           "ref" => "REF_langage_pascal"
       ),
       array(
           "nom" => "Perl",
           "id_geshi" => "Perl",
           "ref" => "REF_langage_Perl"
       ),
       array(
           "nom" => "PHP",
           "id_geshi" => "php",
           "ref" => "REF_langage_php"
       ),
       array(
           "nom" => "Python",
           "id_geshi" => "python",
           "ref" => "REF_langage_python"
       ),
       array(
           "nom" => "Ruby",
           "id_geshi" => "ruby",
           "ref" => "REF_langage_ruby"
       ),
       array(
           "nom" => "Scheme",
           "id_geshi" => "scheme",
           "ref" => "REF_langage_scheme"
       ),
       array(
           "nom" => "SQL",
           "id_geshi" => "sql",
           "ref" => "REF_langage_sql"
       ),
       array(
           "nom" => "XML",
           "id_geshi" => "xml",
           "ref" => "REF_langage_xml"
       ),
       array(
           "nom" => "Assembleur",
           "id_geshi" => "ASM",
           "ref" => "REF_langage_asm"
       ),
       array(
           "nom" => "Scala",
           "id_geshi" => "scala",
           "ref" => "REF_langage_scala"
       ),
       array(
           "nom" => "Makefile",
           "id_geshi" => "Make",
           "ref" => "REF_langage_make"
       ),
       array(
           "nom" => "Images",
           "id_geshi" => "img",
           "ref" => "REF_langage_img"
       )
   );

   public function doLoad(ObjectManager $manager) {
      foreach ($this->data as $l) {
         $langage = new Langage(
                 $l["id_geshi"], $l["nom"]
         );
         $manager->persist($langage);
         $this->addReference($l["ref"], $langage);
      }
      $manager->flush();
   }

   public function getOrder() {
      return 1;
   }

   public function getEnvironments() {
      return array('prod', 'test', 'dev');
   }

}
