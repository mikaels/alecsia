<?php

namespace Alecsia\AnnotationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Alecsia\AnnotationBundle\Entity\Rendu;

/**
 * Générateur de contenu "d'exemple" pour l'application.
 * Références ajoutées :
 * - rendu_mccartney
 * - rendu_lennon
 * - rendu_butler
 */
class LoadRendus extends AbstractDataFixture implements OrderedFixtureInterface {

   private $data = array(
       // Rendu de McCartney
       array(
           "nom" => "tp1_mccartney_harrison.tar.gz",
           "sujet" => "REF_sujet_helloworld",
           'dossier' => '1',
           'note' => 18,
           'termine' => true,
           'students' => array('REF_Student1', 'REF_Student2'),
           'ref_group' => 'REF_UE_PDC_group',
           "ref" => "REF_rendu_mccartney"
       ),
       // // Rendu de Lennon
       array(
           "nom" => "tp2_lennon_starr.tar.gz",
           "sujet" => "REF_sujet_helloworld",
           'dossier' => '2',
           'note' => 2,
           'termine' => true,
           'students' => array('REF_Student3'),
           'ref_group' => 'REF_Group_PDC2',
           "ref" => "REF_rendu_lennon"
       ),
       // // Rendu de Butler
       array(
           "nom" => "tp1_butler_chassagne.tar.gz",
           "sujet" => "REF_sujet_helloworld",
           'dossier' => '3',
           'note' => 11,
           'termine' => false,
           'students' => array(),
           'ref_group' => 'REF_Group_PDC2',
           "ref" => "REF_rendu_butler"
        ),

        array(
            'nom' => 'doe-doe_symfony.tar.gz',
            'sujet' => 'REF_sujet_useless',
            'dossier' => 4,
            'note' => 20,
            'termine' => false,
            'students' => array(),
            'ref_group' => 'REF_Group_PDC2',
            'ref' => 'REF_rendu_doe'
       )
   );

   public function doLoad(ObjectManager $manager) {
      foreach ($this->data as $r) {
         $rendu = new Rendu($r["nom"], $manager->merge($this->getReference($r["sujet"])), $manager->merge($this->getReference($r['ref_group'])));
         $rendu->setDossier($r['dossier']);
         $rendu->setNote($r['note']);
         $rendu->setTermine($r['termine']);
         foreach ($r['students'] as $s) {
            $rendu->addStudent($manager->merge($this->getReference($s)));
         }
         $manager->persist($rendu);
         $this->addReference($r["ref"], $rendu);
      }

      $manager->flush();
   }

   public function getOrder() {
      return 5; // the order in which fixtures will be loaded
   }

   public function getEnvironments() {
      return array('test');
   }

}
