<?php

namespace Alecsia\AnnotationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Alecsia\AnnotationBundle\Entity\Teacher;

/**
 * Générateur de contenu "d'exemple" pour l'application.
 * Références ajoutées :
 */
class LoadTeacher extends AbstractDataFixture implements OrderedFixtureInterface {

   private $data = array(array(
           'login' => 'admin',
           'firstName' => 'Ad',
           'lastName' => 'Mine',
           'email' => 'ad@mine.fr',
           'admin' => true,
           'ref' => 'REF_Teacher_admin',
           'ref_ml' => 'REF_ML_admin',
       ),
       array(
           'login' => 'teacher',
           'firstName' => '',
           'lastName' => '',
           'email' => 'tee@cher.fr',
           'ref' => 'REF_Teacher2',
           'ref_ml' => 'REF_ML_Teacher2'
       ),
       array(
           'login' => 'lazy',
           'firstName' => '',
           'lastName' => '',
           'email' => 'lazy@cher.fr',
           'ref' => 'REF_Teacher3',
           'ref_ml' => 'REF_ML_Teacher3'
       )
   );

   public function doLoad(ObjectManager $manager) {
      foreach ($this->data as $t) {
         $teacher = Teacher::teacherFromMap($t);

         $ml = $this->container->get('alecsia.modelListService')
                 ->createGlobalList($teacher);

         $manager->persist($teacher);
         $this->addReference($t["ref"], $teacher);
         $this->addReference($t["ref_ml"], $ml);
      }
      $manager->flush();
   }

   public function getOrder() {
      return 1;
   }

   public function getEnvironments() {
      return array('test');
   }

}
