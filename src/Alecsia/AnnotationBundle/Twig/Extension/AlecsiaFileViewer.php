<?php

namespace Alecsia\AnnotationBundle\Twig\Extension;

use Alecsia\AnnotationBundle\Service\AlecsiaViewerCapacity;

class AlecsiaFileViewer extends \Twig_Extension {

   protected $kernel = null;

   public function __construct(\AppKernel $kernel) {
      $this->kernel = $kernel;
   }

   public function getFilters() {
      return array(
          'viewAlecsiaFile' => new \Twig_Filter_Method($this, 'twig_viewAlecsiaFile', array('is_safe' => array('html')))
      );
   }

   public function getName() {
      return 'alecsiafileviewer';
   }

   /**
    * View a file
    */
   public function twig_viewAlecsiaFile($content, $id, $filename) {
      return AlecsiaViewerCapacity::view($content, $id, $filename, $this->kernel);
   }

}
