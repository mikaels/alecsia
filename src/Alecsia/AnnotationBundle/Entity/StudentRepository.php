<?php

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\EntityRepository;

class StudentRepository extends EntityRepository {

    const NB_MODES = 3;
   /**
    * Find the potential students who have done the given work
    */
   /* TODO: Traiter le cas des personnes ayant le même nom mais des prénoms
     différents */
   public function findByWork(Rendu $work) {
      $em = $this->getEntityManager();
      $workName = $work->getNameWithoutExtension();

      $dqlQuery = "SELECT DISTINCT s
                 FROM AnnotationBundle:Student s
                 JOIN s.groups grp
                 WHERE (s.lastName IN (:words)
                        OR s.login IN (:words))
                 AND grp.id = :id
                 AND NOT EXISTS
                  (SELECT r FROM AnnotationBundle:Rendu r
                   JOIN r.students stud
                   WHERE r.sujet = :current_subject
                   AND stud.id = s.id)";

      $nb_results = 0;
      $result = array();
        for ($mode = 0; $mode < self::NB_MODES && $nb_results == 0; $mode ++) {
         $potentialLastNames = $this->getPotentialLastNamesFromWorkName($workName, $mode);
         if (!empty($potentialLastNames)) {
            $dqlQ = $em->createQuery($dqlQuery)
                    ->setParameter("words", $potentialLastNames)
                    ->setParameter("id", $work->getGroup()->getId())
                    ->setParameter('current_subject', $work->getSujet()->getId());

            $result = $dqlQ->getResult();
            $nb_results = count($result);
         }
      }
      return $result;
   }

   /**
    * Search students by their last name (or login if not defined)
    */
   public function findByStudentNameOrLogin($name, $firstname = '') {
      $em = $this->getEntityManager();

      $dqlQuery = "SELECT DISTINCT e
                 FROM AnnotationBundle:Student e
                 WHERE (e.lastName LIKE :name AND e.firstName LIKE :firstname)
                 OR (e.lastName IS NULL AND e.firstName IS NULL
                     AND e.login LIKE :name)
                 ORDER BY e.lastName ASC";
      $dqlQuery = $em->createQuery($dqlQuery)
              ->setParameter("name", $name . '%')
              ->setParameter("firstname", $firstname . '%');

      return $dqlQuery->getResult();
   }

   /**
    * @param workName: the name of a work, without the extension
    * @param mode: should we be restrictive or try lots of things? (int. 0: restrictive, 3: not restrictive)
    * @return an array of possibilities
    */
    protected static function getPotentialLastNamesFromWorkName($workName, $mode=0) {
      $regexp = array('/[_\-]+/', // Mode 0
                      '/[_\s\-]+/', // Mode 1
                      '/[_\s\.\-]+/'); // Mode 2

      /* Split everything according to the chosen mode */
      $words = preg_split($regexp[$mode], $workName, -1, PREG_SPLIT_NO_EMPTY);
      return $words;
   }

}
