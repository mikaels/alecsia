<?php

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alecsia\AnnotationBundle\Entity\RegleExclusionFichier
 *
 * @ORM\Table(name="RegleExclusionFichier")
 * @ORM\Entity
 */
class RegleExclusionFichier {

   /**
    * @var integer $id
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
   protected $id;

   /**
    * @var string $regex
    *
    * @ORM\Column(name="regex", type="string", length=255)
    */
   protected $regex;

   /**
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\AlecsiaUser")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
    */
   protected $user;

   function __construct($regex = "", $user = NULL) {
      $this->regex = $regex;
      $this->user = $user;
   }

   /**
    * Get id
    *
    * @return integer
    */
   public function getId() {
      return $this->id;
   }

   public function getRegex() {
      return $this->regex;
   }

   public function setRegex($regex) {
      $this->regex = $regex;
   }

   public function getUser() {
      return $this->user;
   }

   public function setUser($user) {
      $this->user = $user;
   }

}
