<?php

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Alecsia\AnnotationBundle\Entity\ListeModeles
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ListeModeles {

   /**
    * @var integer $id
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
   protected $id;

   /**
    * @ORM\OneToMany(targetEntity="Alecsia\AnnotationBundle\Entity\Modele", mappedBy="liste", cascade={"persist", "remove"})
    */
   protected $modeles;

   public function __construct() {
      $this->modeles = new ArrayCollection();
   }

   public function getId() {
      return $this->id;
   }

   public function getModeles() {
      return $this->modeles;
   }

   public function addModele($modele) {
      $this->modeles[] = $modele;
      $modele->setListe($this);
   }

   /* =========
     = Clone =
     ========= */

   public function cloner($exs) {
      $lm = new ListeModeles();

      $modeles = $this->getModeles();
      foreach ($modeles as $modele) {
         // Association Exos homologues
         $old_ex = $modele->getExercice();
         if ($old_ex == null) {
            $exo = null;
         } else {
            $numex = $old_ex->getNumero();
            $exo = array_filter($exs, function($e) use($numex) {
               return $e->getNumero() == $numex;
            });
            if (count($exo) == 0) {
               $exo = null;
            } else {
               $exo = reset($exo);
            }
         }

         // Copie modèle
         $newm = $modele->cloner($exo, $lm);
         $lm->addModele($newm);
      }
      return $lm;
   }

}
