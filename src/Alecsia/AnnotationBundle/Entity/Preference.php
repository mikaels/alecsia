<?php

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alecsia\AnnotationBundle\Entity\Preference
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Preference {

   /**
    * @var integer $id
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
   protected $id;

   /**
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\AlecsiaUser", cascade={"persist"})
    */
   protected $user;

   /**
    * @var string $cle
    *
    * @ORM\Column(name="cle", type="string", length=255)
    */
   protected $cle;

   /**
    * @var string $valeur
    * @ORM\Column(name="valeur", type="string", length=255)
    */
   protected $valeur;

   /* ================
     = Constructeur =
     ================ */

   function __construct($cle, $valeur, $user) {
      $this->cle = $cle;
      $this->valeur = $valeur;
      $this->user = $user;
   }

   /**
    * Get id
    *
    * @return integer
    */
   public function getId() {
      return $this->id;
   }

   /**
    * Set cle
    *
    * @param string $cle
    */
   public function setCle($cle) {
      $this->cle = $cle;
   }

   /**
    * Get cle
    *
    * @return string
    */
   public function getCle() {
      return $this->cle;
   }

   /**
    * Set valeur
    *
    * @param string $valeur
    */
   public function setValeur($valeur) {
      $this->valeur = $valeur;
   }

   /**
    * Get valeur
    *
    * @return string
    */
   public function getValeur() {
      return $this->valeur;
   }

   public function getUser() {
      return $this->user;
   }

   public function setUser($user) {
      $this->user = $user;
   }

}
