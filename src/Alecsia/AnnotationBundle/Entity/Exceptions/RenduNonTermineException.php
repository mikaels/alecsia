<?php

namespace Alecsia\AnnotationBundle\Entity\Exceptions;

// Déclenché quand on tente de geler un rendu non terminé.

class RenduNonTermineException extends \Exception {

   public function __construct($message = NULL, $code = 0) {
      parent::__construct($message, $code);
   }

}

?>