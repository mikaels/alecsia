<?php

namespace Alecsia\AnnotationBundle\Entity\Exceptions;

/**
 * An exception raised whenever there are some troubles importing an archive.
 */
class ArchiveException extends \Exception {

   private $errors;

   /**
    * @param An associative array containing as many elements as problematic archives.
     Each element has two keys: path -> the filename, error -> the error
     message for that archive
    * @pre The array must not be empty
    */
   public function __construct($errors, $code = 0) {
      $this->errors = $errors;
      $files = array();
      foreach ($errors as $item) {
         $files[] = $item['path'];
      }
      parent::__construct(count($errors) . " archive(s) have not been imported properly: " . join(',', $files) . ".", $code);
   }

   public function getFullErrorMessage() {
      $msg = '';
      foreach ($this->errors as $item) {
         $msg .= $item['path'] . ': ' . $item['error'] . '; ';
      }
      return $msg;
   }

}

?>