<?php

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Alecsia\AnnotationBundle\Entity\Modele;

/**
 * RenduRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RenduRepository extends EntityRepository {

   /**
    * Renvoie les rendus touchés par un modèle.
    */
   public function findByModele(Modele $modele) {
      $em = $this->getEntityManager();

      // Récup des id des rendus concernés
      $dqlQuery = "SELECT DISTINCT r.id
                    FROM AnnotationBundle:Annotation a
                    JOIN a.rendu r
                    WHERE a.modele = :modele
                    ";

      $dqlQuery = $em->createQuery($dqlQuery);
      $dqlQuery->setParameter("modele", $modele->getId());
      $rendusIds = $dqlQuery->getArrayResult();

      if (count($rendusIds) > 0) {
         // Traitement du résultat
         $fct_flat = function($array) {
            return $array["id"];
         };
         $rendusIds = array_map($fct_flat, $rendusIds);

         // Récup des rendus eux mêmes
         $dqlQuery = "SELECT r
                         FROM AnnotationBundle:Rendu r
                         WHERE r.id IN (" . (implode(",", $rendusIds)) . ")";
         $dqlQuery = $em->createQuery($dqlQuery);

         return $dqlQuery->getResult();
      } else {
         return array();
      }
   }

   /**
    * @return all the works on a given subject for a given group
    */
   public function getWorks(Sujet $subject, Group $group) {
      $em = $this->getEntityManager();

      $dqlQuery = "SELECT DISTINCT r FROM AnnotationBundle:Rendu r
                   WHERE r.sujet = :subject AND r.group = :group";
      $dqlQuery = $em->createQuery($dqlQuery)
              ->setParameter('subject', $subject)
              ->setParameter('group', $group);

      return $dqlQuery->getResult();
   }

}
