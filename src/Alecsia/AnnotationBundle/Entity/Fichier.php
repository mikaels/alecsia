<?php

namespace Alecsia\AnnotationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Alecsia\AnnotationBundle\Lib\AlecsiaGlobals;

/**
 * Alecsia\AnnotationBundle\Entity\Fichier
 *
 * @ORM\Table(name="Fichier")
 * @ORM\Entity(repositoryClass="Alecsia\AnnotationBundle\Entity\FichierRepository")
 */
class Fichier {

   /**
    * @var integer $id
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
   protected $id;

   /**
    * @var string $dossier
    *
    * @ORM\Column(name="dossier", type="string", length=255)
    */
   protected $dossier;

   /**
    * @var string $nom_fichier
    *
    * @ORM\Column(name="nom_fichier", type="string", length=255)
    */
   protected $nom_fichier;

   /**
    * @var string $langage
    *
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\Langage")
    */
   protected $langage;

   /**
    * @var integer $taille
    *
    * @ORM\Column(name="taille", type="integer")
    */
   protected $taille;

   /**
    * @var boolean $exclus
    *
    * @ORM\Column(name="exclus", type="boolean")
    */
   protected $exclus;

   /**
    * @ORM\ManyToOne(targetEntity="Alecsia\AnnotationBundle\Entity\Rendu", inversedBy="fichiers", cascade={"persist"})
    */
   protected $rendu;

   /**
    * @pre $fichier is a regular file
    */
   public function __construct($dossier, $fichier, \Alecsia\AnnotationBundle\Entity\Rendu $rendu, \Alecsia\AnnotationBundle\Entity\Langage $langage, $exclus = false) {
      $this->dossier = $dossier;
      $this->nom_fichier = $fichier;

      if (!is_file($this->getCheminAbsolu())) {
         throw new \Exception("Chemin introuvable : " . $this->getCheminAbsolu());
      }

      $this->rendu = $rendu;
      $this->langage = $langage;
      $this->exclus = $exclus;

      // Calcul taille du fichier
      $this->taille = filesize($this->getCheminAbsolu());
   }

   public function exists() {
       return file_exists($this->getCheminAbsolu());
   }

   public function getUploadRootDir() {
      return AlecsiaGlobals::getUploadDir();
   }

   public function getCheminAbsolu() {
      return $this->getUploadRootDir() . "/" . $this->dossier . "/" . $this->nom_fichier;
   }

   /**
    * Ouvre le fichier et renvoie son contenu
    */
   public function getContenu() {
      $fichier = fopen($this->getCheminAbsolu(), "r");

      if (!$fichier) {
         throw new \Exception("Chemin introuvable : " . $this->getCheminAbsolu());
      }

      // On lit ligne par ligne le fichier
      $contenu = "";
      while (!feof($fichier)) {
         $contenu .= fgets($fichier);
      }

      fclose($fichier);

      return $contenu;
   }

   // Getters & Setters
   public function getId() {
      return $this->id;
   }

   public function getNomFichier() {
      return $this->nom_fichier;
   }

   public function getSousDossier() {
      return '/' . preg_replace('/^[0-9]+\/?/', '', $this->dossier);
   }

   public function getDossierRacine() {
      return '/' . preg_replace('/^([0-9]+)\/?.*$/', '$1', $this->dossier);
   }

   public function getTaille() {
      return $this->taille;
   }

   public function setExclus($exclus) {
      $this->exclus = $exclus;
   }

   public function isExclus() {
      return $this->exclus;
   }

   public function getRendu() {
      return $this->rendu;
   }

   public function setRendu(Rendu $rendu) {
      $this->rendu = $rendu;
   }

   public function getLangage() {
      return $this->langage;
   }

   public function setLangage($langage) {
      $this->langage = $langage;
   }

   // Helpers
   public function getSujet() {
      return $this->getRendu()->getSujet();
   }

   // ToString
   public function __toString() {
      return $this->getNomFichier();
   }

}
