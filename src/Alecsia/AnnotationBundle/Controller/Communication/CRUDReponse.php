<?php

namespace Alecsia\AnnotationBundle\Controller\Communication;

use Alecsia\AnnotationBundle\Entity\Annotation;

/*
 * Permet la construction d'une réponse JSON au formulaire de création/modif d'objets
 */

class CRUDReponse {

   private $ajouts = array();
   private $modifs = array();
   private $supprs = array();
   private $notification = null;
   private $erreur = false;

   // Signale l'ajout d'une annotation
   public function ajout($html) {
      $this->ajouts[] = $html;
   }

   // Signale la modif d'une annotation
   public function modif($html) {
      $this->modifs[] = $html;
   }

   // Signale la suppression d'une annotation
   public function suppr($id) {
      $this->supprs[] = $id;
   }

   // Signale une notification : si c'est une erreur, elle annule tous les ajouts/modif/suppr.
   // Il ne peut y avoir qu'une notification.
   public function notification($titre, $contenu, $erreur = false) {
      $notification = array(
          "titre" => $titre,
          "contenu" => $contenu,
          "erreur" => $erreur
      );

      $this->erreur = $erreur;
      $this->notification = $notification;
   }

   /* ===========
     = Getters =
     =========== */

   // Renvoie le JSON correspondant à cette réponse
   public function getJSON() {
      // Effet de l'erreur fatale
      if ($this->erreur) {
         $this->ajouts = array();
         $this->modifs = array();
         $this->supprs = array();
      }

      // Construction de la réponse
      $reponse = array(
          "ajouts" => $this->ajouts,
          "modifs" => $this->modifs,
          "supprs" => $this->supprs,
          "notification" => $this->notification
      );

      return json_encode($reponse);
   }

   // Indique si une erreur est contenue dans la réponse
   public function aUneErreur() {
      return $this->erreur;
   }

}
