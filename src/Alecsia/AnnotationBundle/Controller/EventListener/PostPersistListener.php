<?php

namespace Alecsia\AnnotationBundle\Controller\EventListener;

use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Alecsia\AnnotationBundle\Entity\Teacher;
use Doctrine\ORM\Event\LifecycleEventArgs;

class PostPersistListener extends AlecsiaController {

   protected $manageFileTypesService;
   protected $manageExcludedFiles;
   protected $manageExcludedDirectories;

   public function __construct($manageFileTypesService, $serv_files, $serv_dir) {
      $this->manageFileTypesService = $manageFileTypesService;
      $this->manageExcludedFiles = $serv_files;
      $this->manageExcludedDirectories = $serv_dir;
   }

   public function postPersist(LifecycleEventArgs $args) {
      $entity = $args->getEntity();
      $entityManager = $args->getEntityManager();


      // perhaps you only want to act on some "Product" entity
      if ($entity instanceof Teacher) {
         $this->manageFileTypesService->createDefaultRules($entity);
         $this->manageExcludedFiles->createDefaultRules($entity);
         $this->manageExcludedDirectories->createDefaultRules($entity);
      }
   }

}
