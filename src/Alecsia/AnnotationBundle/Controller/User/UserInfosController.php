<?php

/**
 * Author: Ludovic Loridan
 * Date: 30/01/13
 * Time: 17:19
 *
 * UserInfosController.php
 * Everything that manages the infos of an Alecsia User should
 * pass by this controller.
 */

namespace Alecsia\AnnotationBundle\Controller\User;

use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Alecsia\AnnotationBundle\Service\EntityServices\AlecsiaUserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Form;

class UserInfosController extends AlecsiaController {

   /**
    * @Template("AnnotationBundle:Screenfuls:User/editUserInfos.html.twig")
    */
   public function editUserInfosAction(Request $request) {
      $user = $this->getUser();
      $form = $this->createFormFor($user);

      if ($this->requestIsPOST()) {
         return $this->handleEditionOfUserInfos($form, $request);
      } else {
         return $this->showEditUserInfosForm($form);
      }
   }

   /* ============================== */
   /* Show form                      */
   /* ============================== */

   private function showEditUserInfosForm(Form $form) {
      return array('form' => $form->createView());
   }

   /* ============================== */
   /* Handle form                    */
   /* ============================== */

   private function handleEditionOfUserInfos(Form $form, Request $request) {
      $form->bind($this->getRequest());
      $user = $form->getData();
      $returnedUser = $this->userService()->update($user);
      $this->setFlashMessageFromReturnedEntity($returnedUser, "Modifications enregistrées");
      return $this->redirect($this->getTargetURL());
   }

   /* ============================== */
   /* Create form                    */
   /* ============================== */

   private function createFormFor($user) {
      $fb = $this->createFormBuilder($user);

      $fb->add('lastName', 'text', array("label" => "Nom"))
              ->add('firstName', 'text', array("label" => "Prénom"))
              ->add('email', 'email', array("label" => "Mail"));

      return $fb->getForm();
   }

   /* ============================== */
   /* Redirection                    */
   /* ============================== */

   private function getTargetURL() {
      if ($this->getUser()->isTeacher()) {
         return $this->generateUrl('AlecsiaEditTeacherInfos');
      } else {
         return $this->generateUrl('AlecsiaEditStudentInfos');
      }
   }

   /* ================================== */
   /* Link to services                   */
   /* ================================== */

   /**
    * @return AlecsiaUserService
    */
   private function userService() {
      return $this->get('alecsia.userService');
   }

}
