<?php

/**
 * UserController.php
 * Everything that manages an Alecsia User should
 * pass by this controller.
 */

namespace Alecsia\AnnotationBundle\Controller\User;

use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Alecsia\AnnotationBundle\Service\EntityServices\AlecsiaStudentService;
use Symfony\Component\HttpFoundation\Response;

class UserController extends AlecsiaController {

   public function getStudentsByNameAction($name) {
      $results = array();
      foreach ($this->studentService()->getAllStudentsByName($name) as $student) {
         $results[] = array('firstName' => $student->getFirstName(),
             'lastName' => $student->getLastName(),
             'login' => $student->getLogin());
      }
      return new Response(json_encode($results));
   }

   private function studentService() {
      return $this->get('alecsia.StudentService');
   }

}
