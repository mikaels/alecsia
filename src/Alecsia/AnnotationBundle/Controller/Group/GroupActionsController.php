<?php

namespace Alecsia\AnnotationBundle\Controller\Group;

use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Alecsia\AnnotationBundle\Service\EntityServices\StudentService;
use Alecsia\AnnotationBundle\Entity\Group;
use Alecsia\AnnotationBundle\Entity\UE;
use Alecsia\AnnotationBundle\Entity\AlecsiaUser;

/**
 * Author: Ludovic Loridan
 *
 * GroupActionsController.php
 * Manage all the requests that do modifications to the groups, by doing the actions
 * and doing the correct redirections & flash messages.
 */
class GroupActionsController extends AlecsiaController {

   public function manageUnregisteredStudentsAction() {
      $targetURL = $this->generateUrl('AlecsiaShowActiveUEs');
      $params = $this->getPostParameters();
      $group = $this->GroupService()->get($params["group_id"], $this->getUser());
      if (is_null($group))
         return $this->redirectAndSendError($targetURL, "Impossible de modifier le groupe", "Il n'a pas été trouvé");

      foreach ($params['registered_student'] as $unreg_student => $login) {
         if (!empty($login)) {
            $this->studentService()
                    ->mergeStudents($this->studentService()->getStudentByLogin($login), $this->studentService()->get($unreg_student, $this->getUser()), $this->getUser());
         }
      }

      return $this->redirect($targetURL);
   }

   public function addGroupFromFormAction() {
      /* Get the values from the form */
      $params = $this->getPostParameters();
      $targetURL = $this->generateUrl('AlecsiaTeacherHome');

      $user = $this->UserService()->get($params["user_id"]);
      if (is_null($user))
         return $this->redirectAndSendError($targetURL, "Impossible de créer le groupe", "Le propriétaire pas été trouvé");

      $ue = $this->UEService()->get($params['group_ue_id']);
      if (is_null($ue))
         return $this->redirectAndSendError($targetURL, "Impossible de créer le groupe", "L'UE n'a pas été trouvée");

      if ($ue->getGroups()->count() == 1 && empty($ue->getGroups()->first()->getName())) {
         if (empty($params['first_group_name'])) {
            return $this->redirectAndSendError($targetURL, "Impossible de créer le groupe", "Il faut donner un nom au groupe pré-existant");
         } else {
            /* Rename the exising group */
            $this->renameGroupAction($ue->getGroups()->first(), $params['first_group_name']);
         }
      }

      return $this->addGroupAction($ue, $params['group_name'], $user);
   }

   public function addGroupAction(UE $ue, $group_name, AlecsiaUser $owner) {
      $targetURL = $this->generateUrl('AlecsiaTeacherHome');

      $group = new Group();
      $group->setName($group_name);
      $group->setOwner($owner);
      $ue->addGroup($group);

      /* Create group */
      $groupFromService = $this->GroupService()->add($group, $this->getUser());
      $this->setFlashMessageFromReturnedEntity($groupFromService, "Groupe créé", "Impossible de créer le groupe", "Tous les champs doivent être remplis.");

      /* Update UE */
      $ueFromService = $this->UEService()->update($ue, $this->getUser());
      $this->setFlashMessageFromReturnedEntity($ueFromService, "UE modifiée", "Impossible de modifier l'UE", "Tous les champs doivent être remplis.");

      return $this->redirect($targetURL);
   }

   /**
    * In the form we must have a group_id that gives the ID of the group
    * we must also have a nom that gives the new name
    */
   public function renameGroupFromFormAction() {
      $params = $this->getPostParameters();
      $targetURL = $this->generateUrl('AlecsiaTeacherHome');

      $group = $this->GroupService()->get($params["group_id"], $this->getUser());
      if (is_null($group))
         return $this->redirectAndSendError($targetURL, "Impossible de renommer le groupe", "Le groupe $group_id n'a pas été trouvé");

      return $this->renameGroupAction($group, $params["nom"]);
   }

   /**
    * Rename an existing group
    * @param group: the group to be renamed
    * @param name: the new name of the group
    */
   public function renameGroupAction(Group $group, $name) {
      $group->setName($name);
      /* Update the DB */
      $groupFromService = $this->GroupService()->update($group, $this->getUser());

      return $this->redirect($this->generateUrl('AlecsiaTeacherHome'));
   }

   public function deleteGroupFromIdAction($group_id) {
      $targetURL = $this->generateUrl('AlecsiaTeacherHome');

      $group = $this->GroupService()->get($group_id, $this->getUser());
      if (is_null($group))
         return $this->redirectAndSendError($targetURL, "Impossible de renommer le groupe", "Le groupe $group_id n'a pas été trouvé");
      return $this->deleteGroupAction($group);
   }

   public function deleteGroupAction(Group $group) {
      $this->GroupService()->remove($group, $this->getUser());

      return $this->redirect($this->generateUrl('AlecsiaTeacherHome'));
   }

   public function groupVisibilityAction($group_id, $visibility) {
      $targetURL = $this->generateUrl('AlecsiaTeacherHome');

      $group = $this->GroupService()->get($group_id, $this->getUser());
      if (is_null($group))
         return $this->redirectAndSendError($targetURL, "Impossible de renommer le groupe", "Le groupe $group_id n'a pas été trouvé");

      $group->setViewable($visibility);
      $this->GroupService()->update($group, $this->getUser());

      return $this->redirect($targetURL);
   }

   /**
    * @return GroupService
    */
   private function GroupService() {
      return $this->get('alecsia.GroupService');
   }

   /**
    * @return UEService
    */
   private function UEService() {
      return $this->get('alecsia.UEService');
   }

   /**
    * @return AlecsiaUserService
    */
   private function UserService() {
      return $this->get('alecsia.userService');
   }

   /**
    * @return GroupService
    */
   private function StudentService() {
      return $this->get('alecsia.StudentService');
   }

}
