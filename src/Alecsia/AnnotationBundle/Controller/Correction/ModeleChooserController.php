<?php

namespace Alecsia\AnnotationBundle\Controller\Correction;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Alecsia\AnnotationBundle\Entity\Modele;
use Alecsia\AnnotationBundle\Controller\Preferences\ListeModelesGlobaleController;

class ModeleChooserController extends AlecsiaController {
   /* Renvoie la liste de modèles à proposer en fonction du sujet */

   public function getModelesAction($sujet_id) {

      // Recup BDD du sujet
      $em = $this->getDoctrine()->getManager();
      $sujet = $em->getRepository("AnnotationBundle:Sujet")->findOneById($sujet_id);

      if (!$sujet) {
         throw new NotFoundHttpException(sprintf('Sujet #%s introuvable.', $rendu_id));
      }

      // Recup exo prioritaire
      $exoprior = $this->getRequest()->request->get("exercice_id");

      $modeles = $this->ModelService()->getInterestingModels($sujet, $exoprior);

      /* Get global models */
      $listemodelesGlobaux = $this->ModelListService()
              ->getGlobalList($this->getUser());
      $modeles = array_merge($modeles, $listemodelesGlobaux->getModeles()->toArray());

      $modeles_json = $this->ModelService()
              ->getJsons($modeles, $this, "AnnotationBundle:Correction:Modele/modeletochoose.html.twig");


      // Ajout des options par défaut
      $dataforview = array("abbrev" => "nc", "nom" => "Nouvelle annotation");
      $modeles_json[] = $this->ModelService()
              ->getAbstractJson($this->renderView('AnnotationBundle:Correction:Modele/nouveaucontenu.html.twig', $dataforview), "Nouveau contenu", $dataforview["nom"], $exoprior, -1, "fma_nom", true, false);

      $dataforview = array("abbrev" => "nc", "nom" => "Nouveau modèle dans \"" . $sujet->getNom() . "\"");

      $modeles_json[] = $this->ModelService()
              ->getAbstractJson($this->renderView('AnnotationBundle:Correction:Modele/nouveaucontenu.html.twig', $dataforview), "Nouveau contenu", $dataforview["nom"], $exoprior, -1, "fma_nom", true, false, $sujet->getListeModeles()->getId());

      $dataforview = array("abbrev" => "nc", "nom" => "Nouveau modèle dans \"" . $sujet->getUE()->getNomCourt() . "\"");
      $modeles_json[] = $this->ModelService()
              ->getAbstractJson($this->renderView('AnnotationBundle:Correction:Modele/nouveaucontenu.html.twig', $dataforview), "Nouveau contenu", $dataforview["nom"], -1, -1, "fma_nom", true, true, $sujet->getUE()->getModelList()->getId());

      $dataforview = array("abbrev" => "nc", "nom" => "Nouveau modèle global");
      $modeles_json[] = $this->ModelService()
              ->getAbstractJson($this->renderView('AnnotationBundle:Correction:Modele/nouveaucontenu.html.twig', $dataforview), "Nouveau contenu", $dataforview["nom"], -1, -1, "fma_nom", true, true, $listemodelesGlobaux->getId());

      // Encodage JSON
      $modeles_json = json_encode($modeles_json);

      return new Response($modeles_json);
   }

   private function ModelListService() {
      return $this->get('alecsia.modelListService');
   }

   private function ModelService() {
      return $this->get('alecsia.modeleService');
   }

}
