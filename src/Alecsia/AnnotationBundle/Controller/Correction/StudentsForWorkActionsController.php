<?php

namespace Alecsia\AnnotationBundle\Controller\Correction;

use Alecsia\AnnotationBundle\Controller\AlecsiaController;
use Alecsia\AnnotationBundle\Entity\Annotation;
use Symfony\Component\Config\Definition\Exception\Exception;
use Alecsia\AnnotationBundle\Service\EntityServices\WorkService;
use Alecsia\AnnotationBundle\Entity\Rendu;
use Alecsia\AnnotationBundle\Entity\Sujet;
use Alecsia\AnnotationBundle\Entity\Group;

class StudentsForWorkActionsController extends AlecsiaController {

   public function updateStudentsForWorkAction() {
      $params = $this->getPostParameters();
      $work = $this->getWorkFromParams($params);
      $studentsToRemove = $this->getStudentsToRemoveFromParams($params, $work);
      $studentsToAdd = $this->getStudentsToAddFromParams($params, $work);

      $this->getWorkService()->removeStudentsToWorkIfNecessary($work, $studentsToRemove, $this->getUser());
      $this->getWorkService()->addStudentsToWorkIfNecessary($work, $studentsToAdd, $this->getUser());

      $this->setSuccess("Etudiants modifiés");
      return $this->redirect($this->generateUrl("AnnotationBundle_corriger", array("rendu_id" => $work->getId())));
   }

   /**
    * Add a not registered student to the UE
    */
   public function addStudentForWorkAction() {
      $params = $this->getPostParameters();
      if (empty($params['firstname']) || empty($params['name'])) {
         $this->setError("Paramètres manquants", "Les noms et prénoms de l'étudiant doivent être indiqués");
      }

      $work = $this->getWorkFromParams($params);
      $group = $work->getGroup();

      /* Add the student to the database */
      $student = $this->getStudentService()
              ->addNotRegisteredStudent($params['name'], $params['firstname'], empty($params['email']) ? "" : $params['email']);

      /* Add it to the UE */
      $this->getGroupService()->addStudentInGroup($group, $student, $this->getUser());
      /* Add it to the work */
      $this->getWorkService()
              ->addStudentsToWorkIfNecessary($work, array($student), $this->getUser());

      $this->setSuccess("Etudiant ajouté");
      return $this->redirect($this->generateUrl("AnnotationBundle_corriger", array("rendu_id" => $work->getId())));
   }

   public function updateStudentsInWorksAction($subject_id, $group_id) {
      $em = $this->getDoctrine()->getManager();
      $subject = $em->getRepository('AnnotationBundle:Sujet')->findOneById($subject_id);
      $group = $em->getRepository('AnnotationBundle:Group')->findOneById($group_id);

      if (!$subject) {
         throw new \Exception("Sujet introuvable : " . $subject_id);
      }
      if (!$group) {
         throw new \Exception("Groupe introuvable : " . $group_id);
      }

      $count = $this->addAutomaticallyStudentsToWorks($subject, $group);

      if ($count) {
         if ($count == 1) {
            $this->setSuccess("Un étudiant affecté à un rendu.");
         } else {
            $this->setSuccess($count . " étudiants affectés à des rendus.");
         }
      } else {
         $this->setSuccess("Aucun étudiant affecté à des rendus.");
      }
      return $this->redirect($this->generateUrl("AnnotationBundle_sujet", array("sujet_id" => $subject_id,
                          'group_id' => $group_id)));
   }

   /**
    * Update the works that are not attributed to anyone yet.
    * Try again to find which students they could be attributed to.
    * @return the number of students affected
    */
   public function addAutomaticallyStudentsToWorks(Sujet $subject, Group $group) {
      $em = $this->getDoctrine()->getManager();
      $count = 0;
      foreach ($subject->getRendus() as $work) {
         if ($work->getStudents()->isEmpty() && $work->getGroup() == $group) {
            /* We don't have student yet, search them */
            $students = $em->getRepository('AnnotationBundle:Student')->findByWork($work);
            if (!empty($students)) {
               $count += count($students);
               $this->getWorkService()->addStudentsToWorkIfNecessary($work, $students, $this->getUser());
            }
         }
      }
      return $count;
   }

   /* ============================== */
   /* Param converter                */
   /* ============================== */

   // -- Student --
   private function getStudentsToRemoveFromParams($params, Rendu $work) {
      $new_students = $this->getStudentsFromParams($params);
      $old_students = $work->getStudents()->toArray();
      return array_diff($old_students, $new_students);
   }

   private function getStudentsToAddFromParams($params, $work) {
      $new_students = $this->getStudentsFromParams($params);
      $old_students = $work->getStudents()->toArray();
      return array_diff($new_students, $old_students);
   }

   private function getStudentsFromParams($params) {
      if (isset($params["students"])) {
         $studentsIds = $params["students"];
      } else {
         $studentsIds = array();
      }

      return array_map(array($this, 'getStudentFromId'), $studentsIds);
   }

   private function getStudentFromId($id) {
      $student = $this->getStudentService()->get($id, $this->getUser());
      return $student;
   }

   // -- Work --
   private function getWorkFromParams($params) {
      $work = $this->getWorkService()->get($params["work_id"], $this->getUser());
      if (is_null($work)) {
         throw new Exception('Unreachable Work');
      } else {
         return $work;
      }
   }

   /** @return WorkService */
   private function getWorkService() {
      return $this->get("alecsia.workService");
   }

   private function getGroupService() {
      return $this->get("alecsia.GroupService");
   }

   private function getStudentService() {
      return $this->get("alecsia.studentService");
   }

}
