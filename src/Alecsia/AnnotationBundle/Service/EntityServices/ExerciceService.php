<?php

namespace Alecsia\AnnotationBundle\Service\EntityServices;

/**
 * ExerciceService.php
 *
 */
use Alecsia\AnnotationBundle\Entity\Exercice;

class ExerciceService extends EntityService {

   const managedEntity = 'Alecsia\AnnotationBundle\Entity\Exercice';
   const BONUS_MALUS = "Bonus/Malus";

   /* ============================== */
   /* Initialization                 */
   /* ============================== */

   function __construct($doctrine, $validator) {
      parent::__construct($doctrine, $validator);
   }

   public function getExName($exercice) {
      if (is_null($exercice)) {
         return self::BONUS_MALUS;
      } else {
         return $exercice->__toString();
      }
   }

   public function getExId($exercice) {
      if (is_null($exercice)) {
         return "-1";
      } else {
         return $exercice->getId();
      }
   }

}
