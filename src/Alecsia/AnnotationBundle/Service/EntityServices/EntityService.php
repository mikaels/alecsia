<?php

/**
 * Author: Ludovic Loridan
 * Date: 27/01/13
 * Time: 14:11
 *
 * EntityService.php
 *
 */

namespace Alecsia\AnnotationBundle\Service\EntityServices;

use Doctrine\ORM\EntityRepository;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Validator\Validator;
use Symfony\Component\Validator\ConstraintViolationList;
use Alecsia\AnnotationBundle\Entity\AlecsiaUser;
use Alecsia\AnnotationBundle\Entity\Exceptions\ValidationException;

abstract class EntityService {

   const managedEntity = 'UNDEFINED';

   /* ============================== */
   /* Initialization                 */
   /* ============================== */

   /** @var \Doctrine\Bundle\DoctrineBundle\Registry */
   protected $doctrine;

   /** @var \Symfony\Component\Validator\Validator */
   protected $validator;

   /** @var \Doctrine\ORM\EntityRepository */
   protected $repository;

   function __construct(Registry $doctrine, Validator $validator) {
      $this->doctrine = $doctrine;
      $this->validator = $validator;
   }

   /* ============================== */
   /* Basic accesses                 */
   /* ============================== */

   public function get($id, $user = null) {
      $entity = $this->getRepository()->findOneById($id);
      if (!is_null($entity)) {
         $this->assertUserIsAllowedToGet($entity, $user);
      }
      return $entity;
   }

   /* ============================== */
   /* Basic actions                  */
   /* ============================== */

   // Returns added ue, or error array.
   public function add($entityToAdd, $user = null) {
      $this->assertEntityTypeIsManagedByThisService($entityToAdd);
      $this->assertUserIsAllowedToAdd($entityToAdd, $user);
      $this->validate($entityToAdd);
      if ($this->entityIsValid()) {
         $this->getManager()->persist($entityToAdd);
         $this->getManager()->flush();
         return $entityToAdd;
      } else {
         throw new ValidationException($this->getValidationErrors());
      }
   }

   public function update($entityToUpdate, $user = null) {
      $this->assertEntityTypeIsManagedByThisService($entityToUpdate);
      $this->assertUserIsAllowedToUpdate($entityToUpdate, $user);
      $this->validate($entityToUpdate);
      if ($this->entityIsValid()) {
         $this->getManager()->persist($entityToUpdate);
         $this->getManager()->flush();
         return $entityToUpdate;
      } else {
         throw new ValidationException($this->getValidationErrors());
      }
   }

   public function remove($entityToRemove, $user = null) {
      $this->assertEntityTypeIsManagedByThisService($entityToRemove);
      $this->assertUserIsAllowedToRemove($entityToRemove, $user);
      $this->getManager()->remove($entityToRemove);
      $this->getManager()->flush();
   }

   /* ============================== */
   /* Rights Validation              */
   /* ============================== */

   protected function assertUserIsAllowedToGet($entityToTest, $user) {
      if (!$this->isUserAllowedToGet($entityToTest, $user)) {
         self::throwNotAllowedException("get", $user, $entityToTest);
      }
   }

   protected function assertUserIsAllowedToAdd($entityToTest, $user) {
      if (!$this->isUserAllowedToGet($entityToTest, $user)) {
         self::throwNotAllowedException("add", $user, $entityToTest);
      }
   }

   protected function assertUserIsAllowedToUpdate($entityToTest, $user) {
      if (!$this->isUserAllowedToGet($entityToTest, $user)) {
         self::throwNotAllowedException("update", $user, $entityToTest);
      }
   }

   protected function assertUserIsAllowedToRemove($entityToTest, $user) {
      if (!$this->isUserAllowedToGet($entityToTest, $user)) {
         self::throwNotAllowedException("remove", $user, $entityToTest);
      }
   }

   // --- Error throwing ---
   private static function throwNotAllowedException($notAllowedAction, $user, $entity) {
      $errormsg = sprintf("User '%s' don't have the rights required to %s this '%s'", $user, $notAllowedAction, get_class($entity));
      throw new \Exception($errormsg);
   }

   // --- Defaults methods ---
   private static function isUserAllowedToGet($entityToTest, $user) {
      return self::callMethodOnObjectIfExistsOrReturnValue('isUserAllowedToGet', $entityToTest, $user, true);
   }

   private static function isUserAllowedToAdd($entityToTest, $user) {
      return self::callMethodOnObjectIfExistsOrReturnValue('isUserAllowedToAdd', $entityToTest, $user, true);
   }

   private static function isUserAllowedToUpdate($entityToTest, $user) {
      return self::callMethodOnObjectIfExistsOrReturnValue('isUserAllowedToUpdate', $entityToTest, $user, true);
   }

   private static function isUserAllowedToRemove($entityToTest, $user) {
      return self::callMethodOnObjectIfExistsOrReturnValue('isUserAllowedToRemove', $entityToTest, $user, true);
   }

   private static function callMethodOnObjectIfExistsOrReturnValue($methodName, $object, $parameter, $returnValue) {
      if (method_exists($object, $methodName)) {
         return call_user_func(array($object, $methodName), $parameter);
      } else {
         return $returnValue;
      }
   }

   /* =============================== */
   /* Entity Validation               */
   /* =============================== */

   /* -- Validation of the entity itself -- */

   /** @var ConstraintViolationList */
   private $validationErrors;

   protected function validate($entity) {
      $this->validationErrors = $this->getValidator()->validate($entity);
   }

   protected function entityIsValid() {
      return $this->validationErrors->count() == 0;
   }

   protected function getValidationErrors() {
      return $this->validationErrors;
   }

   /* -- Validation of the entity identity -- */
   /* -- (ex: TeacherService should not be able to manage UEs) -- */

   protected function assertEntityTypeIsManagedByThisService($entity) {
      if (!($this->entityTypeIsManagedByThisService($entity))) {
         $errormsg = sprintf("%s is not able to manage %s classes", $this->getServiceRelativeName(), get_class($entity));
         throw new \Exception($errormsg);
      }
   }

   protected function entityTypeIsManagedByThisService($entity) {
      $targetEntityNamespace = $this->getManagedEntityAbsoluteName();
      return is_a($entity, $targetEntityNamespace);
   }

   /* ============================================== */
   /* Search of the repository                       */
   /* ============================================== */

   private function getServiceRelativeName() {
      $serviceNamespace = get_called_class();
      $serviceNameArray = explode("\\", $serviceNamespace);
      return end($serviceNameArray);
   }

   private function getManagedEntityRelativeName() {
      $entityAbsoluteName = $this->getManagedEntityAbsoluteName();
      $entityAbsoluteNameArray = explode("\\", $entityAbsoluteName);
      return end($entityAbsoluteNameArray);
   }

   private function getManagedEntityAbsoluteName() {
      if (static::managedEntity != 'UNDEFINED') {
         return static::managedEntity;
      } else {
         $errormsg = sprintf('Please provide the managed entity of %s', $this->getServiceRelativeName());
         throw new \Exception($errormsg);
      }
   }

   /**
    * @param $entity: An Entity relative name (eg. UE)
    * @return the corresponding repository
    */
   protected function getEntityRepository($entity) {
      $repositoryNamespace = sprintf('AnnotationBundle:%s', $entity);
      return $this->doctrine->getRepository($repositoryNamespace);
   }

   protected function getRepository() {
      if (is_null($this->repository)) {
         $this->repository = $this->getEntityRepository($this->getManagedEntityRelativeName());
      }
      return $this->repository;
   }

   /* ======================================== */
   /* Access to Services                       */
   /* ======================================== */

   protected function getManager() {
      return $this->doctrine->getManager();
   }

   protected function getValidator() {
      return $this->validator;
   }

}
