<?php

/**
 * Author: Ludovic Loridan
 * Date: 27/01/13
 * Time: 14:10
 *
 * WorkService.php
 *
 */

namespace Alecsia\AnnotationBundle\Service\EntityServices;

use Alecsia\AnnotationBundle\Entity\Rendu;
use Alecsia\AnnotationBundle\Entity\AlecsiaUser;
use Alecsia\AnnotationBundle\Entity\Student;

class WorkService extends EntityService {

   const managedEntity = 'Alecsia\AnnotationBundle\Entity\Rendu';

   /* ============================== */
   /* Initialization                 */
   /* ============================== */

   function __construct($doctrine, $validator) {
      parent::__construct($doctrine, $validator);
   }

   /* ============================== */
   /* Students management            */
   /* ============================== */

   // If necessary = if student isn't already there
   public function addStudentsToWorkIfNecessary(Rendu $work, array $students, AlecsiaUser $user) {
      $this->addStudentsToWorkIfNecessaryWithoutFlush($work, $students);
      $this->update($work, $user);
   }

   public function addStudentsToWorkIfNecessaryWithoutFlush(Rendu $work, array $students) {
      foreach ($students as $student) {
         $this->addStudentToWorkIfNecessaryWithoutFlush($work, $student);
      }
   }

   // If necessary = if student isn't already there
   public function addStudentToWorkIfNecessary(Rendu $work, Student $student, AlecsiaUser $user) {
      $this->addStudentToWorkIfNecessaryWithoutFlush($work, $student);
      $this->update($work, $user);
   }

   protected function addStudentToWorkIfNecessaryWithoutFlush(Rendu $work, Student $student) {
      if (!$work->hasStudent($student)) {
         $this->addStudentToWorkWithoutFlush($work, $student);
      }
      return $work;
   }

   protected function addStudentToWorkWithoutFlush(Rendu $work, Student $student) {
      if ($work->getSujet()->hasStudentWorkedOnIt($student)) {
         throw new \Exception("Impossible d'ajouter cet étudiant à ce rendu : il a déjà rendu sur ce sujet.");
      } else {
         $work->addStudent($student);
      }
   }

   // If necessary = if student isn't already there
   public function removeStudentsToWorkIfNecessary(Rendu $work, array $students, AlecsiaUser $user) {
      foreach ($students as $student) {
         $this->removeStudentToWorkIfNecessaryWithoutFlush($work, $student);
      }
      $this->update($work, $user);
   }

   // If necessary = if student isn't already there
   public function removeStudentToWorkIfNecessary(Rendu $work, Student $student, AlecsiaUser $user) {
      $this->removeStudentToWorkIfNecessaryWithoutFlush($work, $student);
      $this->update($work, $user);
   }

   protected function removeStudentToWorkIfNecessaryWithoutFlush(Rendu $work, Student $student) {
      if ($work->hasStudent($student)) {
         $work->removeStudent($student);
      }
      return $work;
   }

}
