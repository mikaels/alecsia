<?php

namespace Alecsia\AnnotationBundle\Service\EntityServices;

/**
 * Author: Ludovic Loridan
 *
 * SubjectService.php
 *
 */
use Alecsia\AnnotationBundle\Entity\AlecsiaUser;
use Alecsia\AnnotationBundle\Entity\UERepository;
use Alecsia\AnnotationBundle\Entity\UE;
use Alecsia\AnnotationBundle\Entity\Teacher;
use Alecsia\AnnotationBundle\Entity\Sujet;
use Doctrine\ORM\EntityManager;

class SubjectService extends EntityService {

   const managedEntity = 'Alecsia\AnnotationBundle\Entity\Sujet';

   /* ============================== */
   /* Initialization                 */
   /* ============================== */

   function __construct($doctrine, $validator) {
      parent::__construct($doctrine, $validator);
   }

}
