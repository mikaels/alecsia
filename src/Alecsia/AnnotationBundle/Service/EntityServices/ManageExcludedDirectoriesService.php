<?php

namespace Alecsia\AnnotationBundle\Service\EntityServices;

use Alecsia\AnnotationBundle\Entity\RegleExclusionDossier;
use Alecsia\AnnotationBundle\Entity\AlecsiaUser;
use Doctrine\ORM\EntityManager;

class ManageExcludedDirectoriesService extends EntityService {

   const managedEntity = 'Alecsia\AnnotationBundle\Entity\RegleExclusionDossier';

   private $default_excluded = array(
       array(
           "regex" => "/\.svn",
       ),
       array(
           "regex" => "/\.git",
       ),
       array(
           "regex" => "/__MACOSX",
       ),
   );

   function __construct($doctrine, $validator) {
      parent::__construct($doctrine, $validator);
   }

   public function createDefaultRules(AlecsiaUser $user) {
      foreach ($this->default_excluded as $d) {
         $this->add(new RegleExclusionDossier($d['regex'], $user));
      }
   }

}
