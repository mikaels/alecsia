<?php

/**
 * Author: Ludovic Loridan
 * Date: 27/01/13
 * Time: 14:10
 *
 * AlecsiaUserService.php
 * Everything that is common between Teacher and Student should be in this service.
 */

namespace Alecsia\AnnotationBundle\Service\EntityServices;

class AlecsiaUserService extends EntityService {

   const managedEntity = 'Alecsia\AnnotationBundle\Entity\AlecsiaUser';

   /* ============================== */
   /* Initialization                 */
   /* ============================== */

   function __construct($doctrine, $validator) {
      parent::__construct($doctrine, $validator);
   }

   /* ============================== */
   /* Accessors                      */
   /* ============================== */

   public function getUserByLogin($login) {
      return $this->getRepository()->findOneByLogin($login);
   }

   public function userExists($login) {
      return !(is_null($this->getUserByLogin($login)));
   }

}
