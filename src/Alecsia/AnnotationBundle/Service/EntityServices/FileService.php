<?php

namespace Alecsia\AnnotationBundle\Service\EntityServices;

use Alecsia\AnnotationBundle\Entity\Fichier;

class FileService extends EntityService {

   const managedEntity = 'Alecsia\AnnotationBundle\Entity\Fichier';

   /* ============================== */
   /* Initialization                 */
   /* ============================== */

   function __construct($doctrine, $validator) {
      parent::__construct($doctrine, $validator);
   }

   /**
    * @return an array of File that are hidden and that
    * are either in an archived UE or in a frozen subject
    */
   function getOldUnusedFiles() {
       $archived_subjects = $this->getEntityRepository('Sujet')->getOldSubjects();
       $frozen_subjects = $this->getEntityRepository('Sujet')->findByStatus('frozen');

       $old_subjects = array_unique(array_merge($archived_subjects, $frozen_subjects));

       $files = $this->getRepository()->getHiddenFromSubjects($old_subjects);

       $existing_files = array();
       foreach ($files as $file) {
           if ($file->exists())
               $existing_files[] = $file;
       }
       return $existing_files;
   }
}