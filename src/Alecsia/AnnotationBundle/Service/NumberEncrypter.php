<?php

namespace Alecsia\AnnotationBundle\Service;

/**
 * Encrypt numbers.
 */
class NumberEncrypter {

   const INPUT_CHARS = '1234567890';
   const OUTPUT_CHARS = 'nspaorbxme';
   const FAKE_LETTERS = 'cdfghijklqtuvwyz1234567890';
   const STRING_MINIMUM_SIZE = 6;

   /* =============================== */
   /* Encryption                      */
   /* =============================== */

   public function encryptNumber($numberToEncrypt) {
      $string = strval($numberToEncrypt);
      $this->assertStringIsEncryptable($string);
      $string = $this->translateInputCharsToOutputChars($string);
      $string = $this->jamWithFakeLetters($string);
      return $string;
   }

   // --- Input validation ---
   protected function assertStringIsEncryptable($string) {
      for ($i = 0; $i < strlen($string); $i++) {
         $this->assertCharIsEncryptable($string[$i]);
      }
   }

   protected function assertCharIsEncryptable($char) {
      if (strstr(self::INPUT_CHARS, $char) === FALSE) {
         throw new \Exception("Unable to encrypt the string because it is not a plain number");
      }
   }

   // --- Input translation ---
   protected function translateInputCharsToOutputChars($string) {
      return strtr($string, self::INPUT_CHARS, self::OUTPUT_CHARS);
   }

   // --- Input jamming ---
   protected function jamWithFakeLetters($inputString) {
      $nbFakeLetters = $this->getNumberOfFakeLettersRequired($inputString);
      $fakeLetters = $this->getFakeLettersStringOfLength($nbFakeLetters, $inputString);
      return $this->combineStrings($inputString, $fakeLetters);
   }

   protected function getNumberOfFakeLettersRequired($inputString) {
      $nbfakes = self::STRING_MINIMUM_SIZE - strlen($inputString);
      return ($nbfakes > 0) ? $nbfakes : 0;
   }

   protected function getFakeLettersStringOfLength($length, $inputString) {
      $fakeLetterString = '';
      $fakeLettersList = self::FAKE_LETTERS;
      $key = $this->getStringKey($inputString);
      for ($i = 0; $i < $length; $i++) {
         $fakeLetterString .= $this->getFakeLetterAtIndex($key * $i);
      }
      return $fakeLetterString;
   }

   // give a int value which is an hash of the input string
   protected function getStringKey($inputString) {
      $res = 0;
      for ($i = 0; $i < strlen($inputString); $i++) {
         $res += ord($inputString[$i]);
      }
      return $res;
   }

   protected function getFakeLetterAtIndex($index) {
      $fakeLetters = self::FAKE_LETTERS;
      $realindex = $index % strlen($fakeLetters);
      return $fakeLetters[$realindex];
   }

   /**
    * Combine two strings by choosing for each character
    * a character from s1 or s2
    */
   private function combineStrings($s1, $s2) {
      $i_s1 = 0;
      $i_s2 = 0;

      $s1_left = strlen($s1);
      $s2_left = strlen($s2);

      $res = '';
      while ($s1_left > 0 || $s2_left > 0) {

         if ($s1_left == 0) {
            // Add an s2 char
            $res .= $s2[$i_s2];
            $i_s2++;
            $s2_left--;
         } else if ($s2_left == 0) {
            // Add an s1 char
            $res .= $s1[$i_s1];
            $i_s1++;
            $s1_left--;
         } else {
            $shouldBeS2 = ($s2_left % 2) == 0;

            if ($shouldBeS2) {
               // Add an s2 char
               $res .= $s2[$i_s2];
               $i_s2++;
               $s2_left--;
            } else {
               // Add an s1 char
               $res .= $s1[$i_s1];
               $i_s1++;
               $s1_left--;
            }
         }
      }

      return $res;
   }

   /* ============================== */
   /* Decryption                     */
   /* ============================== */

   // Return FALSE if $cryptedString is not valid
   public function decryptString($cryptedString) {
      $supposedNumber = $this->decryptMeaningfulCharsInString($cryptedString);
      $encryptionOfSupposedNumber = $this->encryptNumber($supposedNumber);
      if ($encryptionOfSupposedNumber == $cryptedString) {
         return $supposedNumber;
      } else {
         return FALSE;
      }
   }

   private function decryptMeaningfulCharsInString($cryptedString) {
      $res = '';
      for ($i = 0; $i < strlen($cryptedString); $i++) {
         $res.= $this->decryptChar($cryptedString[$i]);
      }
      return intval($res);
   }

   private function decryptChar($char) {
      $charIndex = strpos(self::OUTPUT_CHARS, $char);
      if ($charIndex !== FALSE) {
         $inputChars = self::INPUT_CHARS;
         return $inputChars[$charIndex];
      } else {
         return FALSE;
      }
   }

}
