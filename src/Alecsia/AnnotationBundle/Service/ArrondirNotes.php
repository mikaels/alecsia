<?php

namespace Alecsia\AnnotationBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\DependencyInjection\ContainerAware;

/**
 * Un service dédié à l'arrondissement de notes
 */
class ArrondirNotes {

   const ARR_SUP = "arrondirSup";
   const ARR_PROCHE = "arrondirProche";
   const ARR_INF = "arrondirInf";
   const ARR_QUART = "quartsProches";
   const ARR_DEMI = "demisProches";
   const ARR_UNITE = "unitesProches";

   /* ============================================
     = Récupération des possibilités d'arrondis =
     ============================================ */

   private function ecartsProches($note, $ecart) {
      $min = floor($note);
      $max = $min + $ecart;
      while (!( $note >= $min && $note < $max)) {
         $min += $ecart;
         $max += $ecart;
      }
      return array($min, $max);
   }

   private function quartsProches($note) {
      return $this->ecartsProches($note, 0.25);
   }

   private function demisProches($note) {
      return $this->ecartsProches($note, 0.5);
   }

   private function unitesProches($note) {
      return $this->ecartsProches($note, 1);
   }

   /* =====================================
     = Choix de la possibilité d'arrondi =
     ===================================== */

   private function arrondirSup($base, $min, $max) {
      if ($base == $min)
         return $min;
      else
         return $max;
   }

   private function arrondirInf($base, $min, $max) {
      if ($base == $max)
         return $max;
      else
         return $min;
   }

   private function arrondirProche($base, $min, $max) {
      $ecartmin = $base - $min;
      $ecartmax = $max - $base;
      if ($ecartmin < $ecartmax) {
         return $min;
      } else {
         return $max;
      }
   }

   /* ===========
     = Arrondi =
     =========== */

   public function arrondirNote($note, $precision, $methode) {
      $proches = $this->$precision($note);
      $arrondi = $this->$methode($note, $proches[0], $proches[1]);
      return $arrondi;
   }

}
