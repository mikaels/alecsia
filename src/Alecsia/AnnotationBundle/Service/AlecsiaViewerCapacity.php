<?php

namespace Alecsia\AnnotationBundle\Service;

/**
 * That class gives information on what type of files Alecsia can view.
 */
class AlecsiaViewerCapacity {

   protected static $registered = array('ImageViewer', 'GeshiViewer');
   protected static $namespace = __NAMESPACE__ . '\Viewers\\';

   /**
    * @param $path is a path to a file
    * @param $id is the id of the language
    * @return true iff the file stored in $path can be nicely viewed through Alecsia.
    *         Nicely means that we have a backend to display it fully and that it won't be too big to be displayed.
    */
   public static function isViewable($path, $id) {
      foreach (self::$registered as $viewer) {
         $result = call_user_func(self::$namespace . $viewer . '::isNicelyViewable', $id, $path);
         if ($result)
            return true;
      }
      return false;
   }

   public static function view($content, $id, $filename, $kernel) {
      foreach (self::$registered as $viewer) {
         if (call_user_func(self::$namespace . $viewer . '::canView', $id)) {
            return call_user_func(self::$namespace . $viewer . '::view', $content, $id, $filename, $kernel);
         }
      }
      return false;
   }

}
