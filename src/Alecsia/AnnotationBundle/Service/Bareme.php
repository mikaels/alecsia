<?php

namespace Alecsia\AnnotationBundle\Service;

use Alecsia\AnnotationBundle\Entity\Rendu;
use Alecsia\AnnotationBundle\Entity\Sujet;
use Alecsia\AnnotationBundle\Entity\Exercice;

/**
 * Structure de données représentant un détail des notes d'un rendu.
 * Ne calcule rien : permet juste de faire transiter des notes.
 */
class Bareme {

   private $sujet;
   private $rendu;
   private $exercices = array();
   private $bonus_malus = 0;
   private $note_finale = 0;

   public function __construct(Rendu $rendu) {
      $this->rendu = $rendu;
      $this->sujet = $rendu->getSujet();
   }

   // SETTERS
   // Ajoute un exercice au détail des notes
   public function ajouterExercice(Exercice $ex, $valeur) {
      $this->exercices[] = array("exercice" => $ex, "note" => $valeur);
   }

   // Précise le bonus/malus
   public function setBonusMalus($valeur) {
      $this->bonus_malus = $valeur;
   }

   // Note du rendu
   public function setNoteFinale($valeur) {
      $this->note_finale = $valeur;
   }

   // GETTERS
   public function getRendu() {
      return $this->rendu;
   }

   public function getSujet() {
      return $this->sujet;
   }

   public function getExercices() {
      return $this->exercices;
   }

   public function getBonusMalus() {
      return $this->bonus_malus;
   }

   public function getNoteFinale() {
      return $this->note_finale;
   }

}
