<?php

namespace Alecsia\AnnotationBundle\Security\User;

use Symfony\Component\Yaml\Yaml;
use Alecsia\AnnotationBundle\Entity\Teacher;
use Alecsia\AnnotationBundle\Entity\Student;
use Alecsia\AnnotationBundle\Entity\AlecsiaUser;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;

class YamlAlecsiaUser {

    private $users;

    function __construct($input) {
        if (empty($input)) {
            $this->users = array();
            return;
        }
        try {
            $this->users = Yaml::parse($input);
            if (!is_array($this->users))
                throw new \Exception();
        } catch (\Exception $e) {
            error_log('Warning unable to read ' . $input . ': ' . $e->getMessage());
            $this->users = array();
        }
    }

    /**
     * @return true iff $user's username is in the array
     */
    function hasUser(AlecsiaUser $user) {
        return array_key_exists($user->getUsername(), $this->users);
    }

    /**
     * @pre hasUser($user)
     * @post $user->getPassword() == password of the user in the JSON file
     */
    function setPassword(AlecsiaUser $user) {
        assert($this->hasUser($user));
        $user->setPassword($this->users[$user->getUsername()]);
    }

}
