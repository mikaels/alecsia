<?php

namespace Alecsia\AnnotationBundle\Security\User\Lille1;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Alecsia\AnnotationBundle\Entity\Teacher;
use Alecsia\AnnotationBundle\Entity\Student;
use Alecsia\AnnotationBundle\Entity\AlecsiaUser;
use Alecsia\AnnotationBundle\Service\EntityServices\TeacherService;
use Alecsia\AnnotationBundle\Service\EntityServices\StudentService;
use Alecsia\AnnotationBundle\Security\User\YamlAlecsiaUser;
use BeSimple\SsoAuthBundle\Security\Core\User\UserFactoryInterface;

class Lille1UserProvider implements UserProviderInterface, UserFactoryInterface {
   /* ================================= */
   /* Core methods                      */
   /* ================================= */

   /** @return AlecsiaUser */
   public function loadUserByUsername($username) {
      $alecsiaTeachers = $this->getTeacherService();
      $lille1Teachers = $this->getLille1TeacherBook();
      $alecsiaStudents = $this->getStudentService();
      $yamlAlecsiaUser = $this->getYamlAlecsiaUser();

      // Get teacher already registered in Alecsia
      if ($alecsiaTeachers->teacherExists($username)) {
         $user = $alecsiaTeachers->getTeacherByLogin($username);
      } else if ($lille1Teachers->teacherExists($username)) {
         $l1teacher = $lille1Teachers->getTeacher($username);
         $user = $alecsiaTeachers->add($l1teacher);
      } else {
         $student = Student::studentFromLogin($username);
         $user = $alecsiaStudents->getExistingStudent($student);
      }
      if ($yamlAlecsiaUser != null && $yamlAlecsiaUser->hasUser($user)) {
         $yamlAlecsiaUser->setPassword($user);
      }
      return $user;
   }

   public function createUser($username, array $roles, array $attributes) {
     $student = Student::studentFromMap(array('login' => $username,
           'firstName' => $attributes['cas:givenname'],
           'lastName' => $attributes['cas:sn'],
           'email' => $attributes['cas:mail'],
           'registered' => true));
     $this->getStudentService()->add($student);
     return $student;
   }

   /** @return AlecsiaUser */
   public function refreshUser(UserInterface $user) {
      return $this->loadUserByUsername($user->getUsername());
   }

   public function supportsClass($class) {
      return ($class instanceof AlecsiaUser);
   }

   /* ======================================= */
   /* User retrieval                          */
   /* ======================================= */


   /* ===================================== */
   /* Link to services                      */
   /* ===================================== */

   /** @var TeacherService */
   private $teacherService;

   /** @var StudentService */
   private $studentService;

   /** @var YamlAlecsiaUser */
   private $yamlAlecsiaUser;

   function __construct($teacherService, $studentService, $lille1TeacherBook, $yamlAlecsiaUser = null) {
      $this->teacherService = $teacherService;
      $this->studentService = $studentService;
      $this->lille1TeacherBook = $lille1TeacherBook;
      $this->yamlAlecsiaUser = $yamlAlecsiaUser;
   }

   /** @return TeacherService */
   private function getTeacherService() {
      return $this->teacherService;
   }

   /** @return StudentService */
   private function getStudentService() {
      return $this->studentService;
   }

   /** @return Lille1TeacherBook */
   private function getLille1TeacherBook() {
      return $this->lille1TeacherBook;
   }

   /** @return YamlAlecsiaUser */
   private function getYamlAlecsiaUser() {
      return $this->yamlAlecsiaUser;
   }

}
