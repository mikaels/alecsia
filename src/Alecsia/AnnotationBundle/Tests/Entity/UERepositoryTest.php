<?php

namespace Alecsia\AnnotationBundle\Tests\Entity;

use Alecsia\AnnotationBundle\Tests\AlecsiaTestCase;

class UERepositoryTest extends AlecsiaTestCase {

   public function testSearchActiveUEEmpty() {
      $lazy = $this->getRepository('AlecsiaUser')
              ->findOneByLogin('lazy');

      $ues = $this->getRepository('UE')
              ->getListOfActiveUEs($lazy);

      $this->assertCount(0, $ues);
   }

   public function testSearchActiveUE() {
      $admin = $this->getRepository('AlecsiaUser')
              ->findOneByLogin('admin');

      $ues = $this->getRepository('UE')
              ->getListOfActiveUEs($admin);

      $this->assertCount(1, $ues);
      $this->assertEquals("PDC", $ues[0]->getNomCourt());
      $this->assertEquals(2014, $ues[0]->getYear());
   }

   public function testSearchArchivedUEEmpty() {
      $teacher = $this->getRepository('AlecsiaUser')
              ->findOneByLogin('teacher');

      $ues = $this->getRepository('UE')
              ->getListOfArchivedUEs($teacher);

      $this->assertCount(0, $ues);
   }

   public function testSearchArchivedUE() {
      $admin = $this->getRepository('AlecsiaUser')
              ->findOneByLogin('admin');

      $ues = $this->getRepository('UE')
              ->getListOfArchivedUEs($admin);

      $this->assertCount(1, $ues);
      $this->assertEquals("PDC", $ues[0]->getNomCourt());
      $this->assertEquals(2013, $ues[0]->getYear());
   }

   /*
     public function testSearchUEWithStudent() {
     $student = $this->getRepository('AlecsiaUser')
     ->findOneByLogin('student1');

     $except = false;

     try {
     $ues = $this->getRepository('UE')
     ->getListOfActiveUEs($student);
     } catch (\PHPUnit_Framework_Error $e) {
     $except = true;
     }
     $this->assertTrue($except);
     } */

   public function testSearchByModel() {
      $ue = $this->em
              ->getRepository('AnnotationBundle:UE')
              ->findOneById(1);

      $this->assertEquals("PDC", $ue->getNomCourt());

      $model = $this->em
              ->getRepository('AnnotationBundle:Modele')
              ->findOneByNom('Makefile manquant');

      $this->assertEquals($ue, $this->em
                      ->getRepository('AnnotationBundle:UE')
                      ->findOneByModel($model));

      /* User-wide model */
      $model = $this->em
              ->getRepository('AnnotationBundle:Modele')
              ->findOneByNom("Login dans le nom de l'archive");

      $this->assertEquals(null, $this->em
                      ->getRepository('AnnotationBundle:UE')
                      ->findOneByModel($model));
   }

}
