<?php

namespace Alecsia\AnnotationBundle\Tests\Entity;

use Alecsia\AnnotationBundle\Tests\AlecsiaTestCase;

class StudentRepositoryTest extends AlecsiaTestCase {

   public function testFindByWorkNone() {
      $work = $this->getRepository('Rendu')
                   ->findOneByNom('tp1_butler_chassagne.tar.gz');
      $students = $this->getRepository('Student')->findByWork($work);
      
      $this->assertCount(0, $students);
   }

   public function testFindByWorkSome() {
      
   }

   public function testFindByWorkMany() {
      $work = $this->getRepository('Rendu')
                   ->findOneByNom('doe-doe_symfony.tar.gz');
      $studentSymfony = $this->getRepository('Student')
                             ->findOneByLogin('etudiant4');
      $studentDoe = $this->getRepository('Student')
                         ->findOneByLogin('student3');
      $students = $this->getRepository('Student')->findByWork($work);

      $this->assertCount(2, $students);
      $this->assertContains($studentSymfony, $students);
      $this->assertContains($studentDoe, $students);
   }

   public function testgetPotentialLastNamesFromWorkName() {
      $method = new \ReflectionMethod('Alecsia\AnnotationBundle\Entity\StudentRepository', 'getPotentialLastNamesFromWorkName');
      $method->setAccessible(TRUE);

      $this->assertEquals(array('Alexandre Bara',
      '201638',
      'assignsubmission',
      'file',
      'TP2',
      'Julien',
      'Hurbain',
      'Alexandre',
      'Bara'),
      $method->invoke(null,'Alexandre Bara_201638_assignsubmission_file_TP2_Julien-Hurbain_Alexandre-Bara', 0));

      $this->assertEquals(array('Alexandre',
      'Bara',
      '201638',
      'assignsubmission',
      'file',
      'TP2',
      'Julien',
      'Hurbain',
      'Alexandre',
      'Bara'),
      $method->invoke(null,'Alexandre Bara_201638_assignsubmission_file_TP2_Julien-Hurbain_Alexandre-Bara',1));

      $this->assertEquals(array('Alexandre',
      'Bara',
      '201638',
      'assignsubmission',
      'file',
      'TP2',
      'Julien',
      'Hurbain',
      'Alexandre',
      'Bara'),
      $method->invoke(null,'Alexandre Bara_201638_assignsubmission_file_TP2_Julien-Hurbain_Alexandre-Bara',2));
   }

   public function testFindByStudentNone() {
      $results = $this->getRepository('Student')
              ->findByStudentNameOrLogin('toto');

      $this->assertCount(0, $results);

      $results = $this->getRepository('Student')
              ->findByStudentNameOrLogin('toto', 'Stue');

      $this->assertCount(0, $results);

      $results = $this->getRepository('Student')
              ->findByStudentNameOrLogin('Alecsia');

      $this->assertCount(0, $results);

      $results = $this->getRepository('Student')
              ->findByStudentNameOrLogin('%z');

      $this->assertCount(0, $results);
   }

   public function testFindByStudentNameSome() {
      $results = $this->getRepository('Student')
              ->findByStudentNameOrLogin('%e');

      $this->assertCount(3, $results);

      $names = array_map(function ($obj) {
         return $obj->getAbsoluteDisplayName();
      }, $results);

      $this->assertContains('Stue Dent', $names);
      $this->assertContains('Stiou Dent', $names);
      $this->assertContains('Jane Doe', $names);

      $results = $this->getRepository('Student')
              ->findByStudentNameOrLogin('Symfony');
      $this->assertCount(1, $results);
      $this->assertEquals('Symfony', $results[0]->getLastName());
      $this->assertEquals('Alecsia', $results[0]->getFirstName());
   }

   public function testFindByStudentLoginSome() {
      $results = $this->getRepository('Student')
              ->findByStudentNameOrLogin('%e');

      $this->assertCount(3, $results);

      $names = array_map(function ($obj) {
         return $obj->getAbsoluteDisplayName();
      }, $results);

      $this->assertContains('Stue Dent', $names);
      $this->assertContains('Stiou Dent', $names);
      $this->assertContains('Jane Doe', $names);
   }

}
