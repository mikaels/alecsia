<?php

namespace Alecsia\AnnotationBundle\Tests\Service;

use Alecsia\AnnotationBundle\Tests\AlecsiaTestCase;
use Alecsia\AnnotationBundle\Service\AlecsiaUtils;

class AlecsiaUtilsTest extends AlecsiaTestCase
{

    public function testRemoveAccents() {
        $this->assertEquals(AlecsiaUtils::remove_accents('àÀÉçÇå'), 'aAEcCa');
        
        /* ISO-8859-1 */
        $this->assertEquals(AlecsiaUtils::remove_accents(stripcslashes('\xe0\xe5\xe9\xd3'), 'iso-8859-1'), 'aaeO');
    }
    
}
