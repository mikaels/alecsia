# Alecsia

Alecsia is a web application dedicated to annotating and marking
student digital works.

## Rationale

Marking or annotating student works can be a very time-consuming task.
Alecsia provides several ways to save time. It allows to reuse
annotations that have been previously entered. Alecsia also allows to
provide relative marks which permits to fine-tune the mark scheme
afterwards.

The students also benefit from this application. First all their works
are available, and commented, in a unique place. Second, thanks to
Alecsia's ability to reuse an annotation with different students, the
works are marked more uniformly across students.

## Implementation

Alecsia is implemented as a web application relying on Symfony2.
The original implementation is due to Ludovic Loridan.

You can see on a [continuous
integration](https://ci.inria.fr/alecsia/) server if the builds are
passing.

## License

Alecsia is released under a GPL-compilant CeCILL license
(for more details see the [license file](LICENSE)).

## Installation

See the [installation file](INSTALLATION.md)
